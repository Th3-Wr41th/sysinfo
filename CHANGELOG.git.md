# Changelog

All changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v0.3.0] - 2024-01-10 

### Bug Fixes

- **3b38e58** - replace pacman args with the correct ones
- **f851ba9** - remove borrow from `FsType` comparison
- **eadfbde** - apply clippy changes
- **778bbb8** - discard filesystems that cannot be instantiated
- **6831a20** - move IecUnits import from `lib_sysinfo::general` to `lib_sysinfo::units`
- **fd778b7** - specify into type to prevent errors when not specified
- **b3b4c21** - change visibility of cmd fn to prevent leaking an internal function

### Tests

- **406744c** - refactor mockfs to explicitly define test files and simplify macros
- **887fca1** - add mockfs data for MTAB to allow tests to compile
- **89ea370** - update doctests and examples to reflect api changes

### Documentation

- **fe4b36b** - mark packages section as complete
- **514a9a9** - update readme with todo list and ideas for the future
- **bead068** - update git changelog and template
- **4199aff** - replace git-chglog with changelog
- **eb1f9a2** - update git changelog
- **bc6c575** - update chglog template to use title maps
- **0c6cef0** - add missing manifest metadata
- **318b9d4** - group entries by scope and fix typos
- **0241011** - add missing licenses and update license notices
- **60287ca** - update readme features checklist
- **35d7846** - update changelogs
- **c308450** - auto generate a variant of the changelog for git commits
- **5eedfc6** - update README with updated features checklist
- **2fa5bdf** - add documentation to uptime and loadavg functions
- **962fee1** - update README with updated features checklist
- **eec4d2f** - fix typo in copyright notice
- **17e4ced** - update features checklist
- **2a0c355** - update changelog
- **28d01ab** - replace features list with a checklist
- **04325ee** - rename license files
- **5f018b2** - add missing date for the 0.2.0 release
- **de62123** - remove newline from the middle of the sentence
- **4036a0b** - add changelog
- **ae1defb** - add license badges to README.md
- **d04e49b** - fill in details for the MIT license
- **06fb01f** - add copy of licenses to the root
- **f862e4b** - add pipeline badge to README.md

### Features

- **b57372a** - add support for various common filesystem types
- **94d9066** - change format of disk usage
- **88bc0d0** - add caching to `Fs::parse_mtab`
- **e482bb4** - add required traits to structs and display impl for `Fs`
- **c3918e4** - add support for show filesystem usage
- **57a5392** - add implementations for Fs methods
- **c8adf57** - add getter functions in statfs fields
- **c35cd01** - add statfs fs implementation
- **8068329** - scaffold fs struct for future disk info feature
- **9a02285** - add support for displaying memory and swap usage
- **045064d** - re-export unit_system traits and derive macros
- **c5ab7be** - update example for reflect changes
- **f1460aa** - add fn meminfo to get memory usage information
- **9bb9806** - impl unit system for converting bytes using IEC units
- **07409a4** - add unit system
- **1c75682** - add support for displaying interface state
- **c933aa0** - add interface state and sort interfaces by ifindex
- **7e88447** - add support for getting interfaces with ip addresses
- **8123978** - add support for displaying processes
- **6354723** - add fn processes to get number running process
- **a1437ac** - add support for displaying load averages
- **9c7ea78** - add display impl to LoadAvg struct
- **2f3f594** - add loadavg fn to read load averages from /proc/loadavg
- **46a00dc** - add uptime fn to format btime into hh:mm:ss
- **7ff6823** - add fn to get cpu architecture
- **8a732b2** - add OS fallback to distribution
- **b9d4238** - show ip address type before ip address

### CI/CD

- **a449960** - update test-lib job to allow dead_code in tests
- **3a3d5b4** - disable doc tests for lib_sysinfo
- **0c07f21** - fix tests not passing due to missing pci id database
- **84ee602** - add .gitlab-ci.yml file

### Code Refactoring

- **045f2cd** - gate modules behind feature flags
- **0034a20** - move `Database::read` out of `parse_data` function
- **cca179e** - move units from crate::general to crate
- **b5aa460** - support displaying interfaces without ip addresses
- **2ed84b3** - simplify allow attributes
- **0302b2e** - remove unnecessary dead_code attributes
- **9c12a5c** - use `Arc<str>` instead `String`
- **74a5a8b** - prevent the read macro from panicking it cannot read the file
- **7ae88ab** - move rpm frontends to a const slice

### Chores

- **de1e334** - bump version of sysinfo and lib_sysinfo to 0.3.0
- **5cb6fe2** - update changelogs
- **48be727** - update git changelog

## [v0.2.0] - 2024-01-04 

### Chores

- **d2ede98** - update dependencies and bump version to 0.2.0

### Code Refactoring

- **8b0c06f** - update mockfs data and docs to reflect api changes
- **860d789** - change raw formatter to use writeln macros
- **2a812ed** - change data display implementations to use fmt::Formatter

### Bug Fixes

- **c4be53f** - add serde attributes to properly tag Ip enum variants
- **279b877** - add attributes to fix conditional compilation

### Features

- **fbf726d** - rewrite network module
- **88c6094** - add support for man page generation

### Other

- **79c326b** - add info to manifest files
- **01c719a** - fix license info
- **e21615f** - add readme

## [v0.1.0] - 2024-01-04 

### Other

- **62983c1** - initial commit

[Unreleased]: https://gitlab.com/Th3-Wr41th/sysinfo/compare/v0.3.0...HEAD
[v0.3.0]: https://gitlab.com/Th3-Wr41th/sysinfo/compare/v0.2.0...v0.3.0
[v0.2.0]: https://gitlab.com/Th3-Wr41th/sysinfo/compare/v0.1.0...v0.2.0
[v0.1.0]: https://gitlab.com/Th3-Wr41th/sysinfo/commits/v0.1.0
