# sysinfo

[![pipeline status](https://gitlab.com/Th3-Wr41th/sysinfo/badges/master/pipeline.svg)](https://gitlab.com/Th3-Wr41th/sysinfo/-/commits/master) [![License](https://img.shields.io/badge/License-MIT-blue)](https://gitlab.com/Th3-Wr41th/sysinfo/-/blob/master/LICENSE_MIT) [![License](https://img.shields.io/badge/License-MPL--2.0-blue)](https://gitlab.com/Th3-Wr41th/sysinfo/-/blob/master/LICENSE_MPL-2.0)

Yet another command line program to gather system information.

## Features

- [x] General
  - [x] Kernel Version
  - [x] Distribution
  - [x] Uptime
  - [x] Processes
  - [x] Load Averages
  - [x] Memory Usage
- [x] Packages
  - [x] Rpm (dnf or yum)
  - [x] Pacman
  - [x] Dpkg (apt)
  - [x] Cargo
- [x] Network
  - [x] Interfaces (with IP addresses)
  - [x] Interfaces (without IP addresses)
  - [x] IPv4 (with CIDR)
  - [x] IPv6 (with prefix)
  - [x] Mac Addresses
  - [x] Interface Types
  - [x] Interface State
  - [x] Sort by interface index
- [x] Hardware
  - [x] Host
  - [x] BIOS
  - [x] CPU(s)
    - [x] Architecture
  - [x] GPU(s)
  - [x] Audio devices
  - [x] Network devices
- [x] Disks
  - [x] Name
  - [x] Total Size
  - [x] Available Space
  - [x] File System
  - [x] Mount Point

## Todo

- Documentation
- Examples

## Ideas

- Add package frontend as a config option
    - Allow users to specify which package manager front ends to use, may enable support for the `aur` on arch.
- Support other packaging formats
    - Flatpak

## Notes

`lib_sysinfo` requires glibc version 2.38

`CHANGELOG.git.md` is built with [`changelog`](https://gitlab.com/Th3-Wr41th/changelog)

## License

`packages/sysinfo` is licensed under the [Mozilla Public License 2.0](LICENSE_MPL-2.0)

`packages/lib_sysinfo` is licensed under the [MIT License](LICENSE_MIT)

`packages/unit_system` is licensed under the [MIT License](LICENSE_MIT)

`packages/unit_system_macros` is licensed under the [MIT License](LICENSE_MIT)
