// SPDX-License-Identifier: MIT

use proc_macro2::TokenStream;
use syn::{Data, DeriveInput, Fields, Ident};

// #[derive(UnitSystem)]
// #[unit_system(add, sub)]
#[derive(deluxe::ExtractAttributes)]
#[deluxe(attributes(unit_system))]
struct OuterAttributes {
    add: Option<bool>,
    sub: Option<bool>,
}

pub fn derive(input: TokenStream) -> deluxe::Result<TokenStream> {
    let mut ast: DeriveInput = syn::parse2(input)?;

    let OuterAttributes { add, sub } = deluxe::extract_attributes(&mut ast)?;

    match ast.data {
        Data::Struct(data_struct) => match data_struct.fields {
            Fields::Unnamed(_) => (),
            _ => panic!("Invalid data type. Only valid on tuple structs"),
        },
        _ => panic!("Invalid data type. Only valid on tuple structs"),
    }

    let ident: Ident = ast.ident;
    let (impl_generics, type_generics, where_clause) = ast.generics.split_for_impl();

    let add_tokens = if add.is_some() {
        quote! {
            impl #impl_generics std::ops::Add for #ident #type_generics #where_clause {
                type Output = Self;

                fn add(self, other: Self) -> Self::Output {
                    <Self as unit_system::UnitSystem>::new(<Self as unit_system::UnitSystem>::value(&self) + <Self as unit_system::UnitSystem>::value(&other))
                }
            }
        }
    } else {
        quote! {}
    };

    let sub_tokens = if sub.is_some() {
        quote! {
            impl #impl_generics std::ops::Sub for #ident #type_generics #where_clause {
                type Output = Self;

                fn sub(self, other: Self) -> Self::Output {
                    <Self as unit_system::UnitSystem>::new(<Self as unit_system::UnitSystem>::value(&self) - <Self as unit_system::UnitSystem>::value(&other))
                }
            }
        }
    } else {
        quote! {}
    };

    Ok(quote! {
        impl #impl_generics UnitSystem for #ident #type_generics #where_clause {
            fn new<V: Into<f64>>(val: V) -> Self {
                Self(val.into())
            }

            fn from_unit<V, U>(value: V, unit: U) -> Self
            where
                V: Into<f64>,
                U: unit_system::Unit,
            {
                // Convert to bytes
                Self((value.into()) * unit.value())
            }

            fn value(&self) -> f64 {
                self.0
            }
        }

        #add_tokens

        #sub_tokens
    })
}
