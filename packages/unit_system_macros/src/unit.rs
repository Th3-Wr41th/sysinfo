// SPDX-License-Identifier: MIT

use proc_macro2::{Span, TokenStream};
use syn::{Data, DeriveInput, Ident};

// #[derive(Unit)]
// #[unit(base = 1024)]
#[derive(deluxe::ExtractAttributes)]
#[deluxe(attributes(unit))]
struct OuterAttributes {
    base: u32,
}

#[derive(deluxe::ExtractAttributes)]
#[deluxe(attributes(unit))]
// #[rename = "kB"]
struct FieldAttributes {
    rename: Option<String>,
}

#[derive(Debug)]
struct Variant {
    ident: Ident,
    name: String,
    value: f64,
}

fn extract_variant_attributes(ast: &mut DeriveInput, base: f64) -> deluxe::Result<Vec<Variant>> {
    let mut variants: Vec<Variant> = Vec::new();

    if let Data::Enum(data) = &mut ast.data {
        for (idx, variant) in data.variants.iter_mut().enumerate() {
            let index: u16 = u16::try_from(idx).expect("failed to construct u16 from usize");

            let ident = variant.ident.clone();

            let FieldAttributes { rename } = deluxe::extract_attributes(variant)?;

            let name = match rename {
                Some(name) => name,
                None => ident.to_string(),
            };

            let value = base.powi(i32::from(index));

            variants.push(Variant { ident, name, value });
        }
    }

    Ok(variants)
}

pub fn derive(input: TokenStream) -> deluxe::Result<TokenStream> {
    let mut ast: DeriveInput = syn::parse2(input)?;

    let OuterAttributes { base } = deluxe::extract_attributes(&mut ast)?;

    let base: f64 = f64::from(base);

    let variants_vec: Vec<Variant> = extract_variant_attributes(&mut ast, base)?;

    let mut variants: Vec<Ident> = Vec::new();
    let mut units: Vec<&str> = Vec::new();
    let mut values: Vec<f64> = Vec::new();

    for variant in &variants_vec {
        variants.push(variant.ident.clone());
        units.push(&variant.name);
        values.push(variant.value);
    }

    if (values[0] - 1f64).abs() > f64::EPSILON {
        let err = syn::Error::new(Span::call_site(), "First value must be 1f64");

        return Err(err);
    }

    let ident: Ident = ast.ident;
    let (impl_generics, type_generics, where_clause) = ast.generics.split_for_impl();

    Ok(quote! {
        #[automatically_derived]
        impl #impl_generics Unit for #ident #type_generics #where_clause {
            const BASE: f64 = #base;

            fn value(&self) -> f64 {
                match self {
                    #(Self::#variants => #values,)*
                }
            }

            fn units<'a>() -> &'a [Self] {
                &[
                    #(Self::#variants,)*
                ]
            }

            fn unit<'a>(&self) -> &'a str {
                match self {
                    #(Self::#variants => #units,)*
                }
            }
        }
    })
}
