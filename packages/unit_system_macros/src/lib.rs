// SPDX-License-Identifier: MIT

#[macro_use]
extern crate quote;

mod unit;
mod unit_system;

use proc_macro::TokenStream;

macro_rules! handle_err {
    ($expr:expr) => {
        match $expr {
            Ok(ts) => ts.into(),
            Err(err) => err.to_compile_error().into(),
        }
    };
}

#[proc_macro_derive(UnitSystem, attributes(unit_system))]
pub fn derive_unit_system(input: TokenStream) -> TokenStream {
    handle_err!(unit_system::derive(input.into()))
}

#[proc_macro_derive(Unit, attributes(unit))]
pub fn derive_unit(input: TokenStream) -> TokenStream {
    handle_err!(unit::derive(input.into()))
}
