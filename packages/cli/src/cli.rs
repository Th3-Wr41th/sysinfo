// SPDX-License-Identifier: MPL-2.0

use clap::{Parser, ValueEnum};
use serde::Deserialize;
use std::fmt;
use std::path::PathBuf;
use strum::EnumIter;

#[cfg(feature = "fmt_bat")]
use bat::PagingMode as BatPagingMode;

/// Gather information about your system
#[derive(Debug, Deserialize, Parser)]
#[serde(rename_all = "lowercase")]
#[command(author, version, about, long_about = None)]
pub struct Args {
    /// Output format
    #[arg(short, long, value_enum)]
    pub format: Option<Format>,

    /// Only show specific sections
    #[arg(short, long, value_enum, value_delimiter = ',', num_args = 1..)]
    #[serde(default)]
    pub sections: Vec<Section>,

    /// Do not print progress bars
    #[arg(short, long)]
    #[serde(default)]
    pub quiet: bool,

    /// When to use a pager (Only supported using the bat formatter)
    #[cfg(feature = "fmt_bat")]
    #[serde(default)]
    #[arg(short, long, value_enum)]
    pub paging: Option<PagingMode>,

    /// Do not reorder sections according to `--sections`
    #[arg(long)]
    #[serde(default)]
    pub no_reorder: bool,

    /// Use alternate config file
    #[arg(long)]
    #[serde(skip)]
    pub config: Option<PathBuf>,

    /// Do not load any config file
    #[arg(long)]
    #[serde(skip)]
    pub no_config: bool,

    /// Generate man pages
    #[cfg(feature = "mangen")]
    #[serde(skip)]
    #[arg(long)]
    pub mangen: bool,
}

impl Args {
    pub fn merge(&self, other: &Self) -> Self {
        let sections = if other.sections.is_empty() {
            &self.sections
        } else {
            &other.sections
        };

        let config = if other.config.is_some() {
            other.config.clone()
        } else {
            None
        };

        Self {
            format: other.format.or(self.format),
            sections: sections.clone(),
            quiet: other.quiet | self.quiet,
            #[cfg(feature = "fmt_bat")]
            paging: other.paging.or(self.paging),
            no_reorder: other.no_reorder | self.no_reorder,
            config,
            no_config: other.no_config | self.no_config,
            #[cfg(feature = "mangen")]
            mangen: false,
        }
    }
}

#[derive(Debug, Deserialize, ValueEnum, Clone, Copy)]
#[serde(rename_all = "lowercase")]
pub enum Format {
    Raw,
    #[cfg(feature = "fmt_bat")]
    Bat,
    #[cfg(feature = "fmt_json")]
    Json,
    #[cfg(feature = "fmt_toml")]
    Toml,
}

impl Default for Format {
    #[cfg(feature = "fmt_bat")]
    fn default() -> Self {
        Self::Bat
    }

    #[cfg(not(feature = "fmt_bat"))]
    fn default() -> Self {
        Self::Raw
    }
}

#[derive(Debug, Deserialize, ValueEnum, Clone, Copy, EnumIter)]
#[serde(rename_all = "lowercase")]
#[repr(usize)]
pub enum Section {
    General = 0usize,
    Packages,
    Networks,
    Disks,
    Hardware,
}

impl fmt::Display for Section {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::General => write!(f, "General"),
            Self::Packages => write!(f, "Packages"),
            Self::Hardware => write!(f, "Hardware"),
            Self::Networks => write!(f, "Networks"),
            Self::Disks => write!(f, "Disks"),
        }
    }
}

#[derive(Debug, Deserialize, ValueEnum, Clone, Copy)]
#[serde(rename_all = "lowercase")]
pub enum PagingMode {
    Always,
    Contextual,
    Never,
}

impl Default for PagingMode {
    fn default() -> Self {
        Self::Contextual
    }
}

#[cfg(feature = "fmt_bat")]
impl From<PagingMode> for BatPagingMode {
    fn from(val: PagingMode) -> Self {
        match val {
            PagingMode::Always => Self::Always,
            PagingMode::Contextual => Self::QuitIfOneScreen,
            PagingMode::Never => Self::Never,
        }
    }
}
