// SPDX-License-Identifier: MPL-2.0

#![cfg(feature = "fmt_json")]

use anyhow::Context;
use std::collections::HashMap;

use crate::data::SectionData;

pub fn format(data: &[SectionData], _args: &crate::cli::Args) -> anyhow::Result<()> {
    let data: HashMap<&str, &SectionData> = data
        .iter()
        .map(|section| (section.name(), section))
        .collect();

    let json = serde_json::to_string_pretty(&data).context("failed to parse to json")?;

    println!("{json}");

    Ok(())
}
