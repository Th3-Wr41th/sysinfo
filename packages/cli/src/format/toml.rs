// SPDX-License-Identifier: MPL-2.0

#![cfg(feature = "fmt_toml")]

use anyhow::Context;
use std::collections::HashMap;

use crate::data::SectionData;

pub fn format(data: &[SectionData], _args: &crate::cli::Args) -> anyhow::Result<()> {
    let data: HashMap<&str, &SectionData> = data
        .iter()
        .map(|section| (section.name(), section))
        .collect();

    let toml = toml::to_string_pretty(&data).context("failed to parse to json")?;

    println!("{toml}");
    Ok(())
}
