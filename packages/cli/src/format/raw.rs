// SPDX-License-Identifier: MPL-2.0

use crate::data::SectionData;

use std::fmt::Write;

pub fn format(data: &[SectionData], _args: &crate::cli::Args) -> anyhow::Result<()> {
    let mut s = String::new();

    let sep = "=".repeat(80);

    for section in data {
        writeln!(s, "{sep}")?;
        writeln!(s)?;

        writeln!(s, "{0}", section.title())?;
        writeln!(s)?;

        writeln!(s, "{section}")?;
    }
    writeln!(s, "{sep}")?;

    print!("{s}");

    Ok(())
}
