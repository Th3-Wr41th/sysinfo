// SPDX-License-Identifier: MPL-2.0

#[cfg(feature = "fmt_bat")]
mod bat;
#[cfg(feature = "fmt_json")]
mod json;
mod raw;
#[cfg(feature = "fmt_toml")]
mod toml;

#[cfg(feature = "fmt_bat")]
pub use bat::format as bat;
#[cfg(feature = "fmt_json")]
pub use json::format as json;
pub use raw::format as raw;
#[cfg(feature = "fmt_toml")]
pub use toml::format as toml;
