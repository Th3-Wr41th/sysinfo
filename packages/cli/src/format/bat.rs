// SPDX-License-Identifier: MPL-2.0

#![cfg(feature = "fmt_bat")]

use anyhow::Context;
use bat::{Input, PrettyPrinter};

use crate::{cli::Args, data::SectionData};

pub fn format(data: &[SectionData], args: &Args) -> anyhow::Result<()> {
    let data: Vec<(&str, Vec<u8>)> = data
        .iter()
        .map(|data| (data.title(), data.as_bytes()))
        .collect();

    let paging_mode = args.paging.unwrap_or_default();

    PrettyPrinter::new()
        .header(true)
        .grid(true)
        .line_numbers(true)
        .paging_mode(paging_mode.into())
        .inputs(
            data.iter()
                .map(|(title, data)| Input::from_bytes(data).kind("Section").title(*title)),
        )
        .print()
        .context("failed to print output")?;

    Ok(())
}
