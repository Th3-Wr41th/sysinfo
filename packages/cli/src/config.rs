// SPDX-License-Identifier: MPL-2.0

use anyhow::Context;
use std::fs;
use std::env;
use std::path::PathBuf;

use crate::cli::Args;

pub fn get_config_path() -> Option<PathBuf> {
    let xdg: Option<PathBuf> = env::var_os("XDG_CONFIG_HOME").map(PathBuf::from);

    let path: PathBuf = xdg.or_else(|| {
        env::var_os("HOME")
            .map(PathBuf::from)
            .map(|h| h.join(".config"))
    })?;

    let file = path.join("sysinfo/config.toml");

    if file.exists() {
        Some(file)
    } else {
        None
    }
}

fn load_config(file: &PathBuf) -> anyhow::Result<Args> {
    let buf = fs::read_to_string(file).context(format!("failed to read file: {file:#?}"))?;

    let data = toml::from_str(&buf).context("failed to parse config file")?;

    Ok(data)
}

pub fn try_merge_config(args: &Args) -> anyhow::Result<Option<Args>> {
    let config = args.config.clone();

    let path: PathBuf = if let Some(config) = config {
        config
    } else if let Some(config_path) = get_config_path() {
        config_path
    } else {
        return Ok(None);
    };

    let config = load_config(&path)?;

    Ok(Some(config.merge(args)))
}


