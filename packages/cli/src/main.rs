// SPDX-License-Identifier: MPL-2.0

mod cli;
mod config;
mod data;
mod format;

#[cfg(feature = "mangen")]
mod mangen;

use anyhow::Context;
use clap::Parser;
use indicatif::{MultiProgress, ProgressDrawTarget};
use std::collections::HashMap;
use strum::IntoEnumIterator;
use tokio::task::JoinSet;

use cli::{Args, Format, Section};
use config::try_merge_config;
use data::SectionData;

const INDENT: &str = "  ";

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let mut args: Args = Args::parse();

    #[cfg(feature = "mangen")]
    {
        if args.mangen {
            mangen::mangen().context("failed to generate man page")?;

            return Ok(());
        }
    }

    if !args.no_config {
        if let Some(new_args) = try_merge_config(&args)? {
            args = new_args;
        }
    }

    let multi = if args.quiet {
        MultiProgress::with_draw_target(ProgressDrawTarget::hidden())
    } else {
        MultiProgress::new()
    };

    let mut set = JoinSet::new();

    let mut process_section = |section: &Section| {
        let multi = multi.clone();
        let section = *section;

        set.spawn(async move { SectionData::get(section, multi) });
    };

    if args.sections.is_empty() {
        let _: Vec<_> = Section::iter()
            .map(|section| process_section(&section))
            .collect();
    } else {
        let _: Vec<_> = args.sections.iter().map(process_section).collect();
    };

    let mut data: Vec<SectionData> = Vec::new();

    while let Some(res) = set.join_next().await {
        let section_data = res.context("task failed to execute to completion")??;

        data.push(section_data);
    }

    if !args.sections.is_empty() && !args.no_reorder {
        let data_indexed: HashMap<usize, SectionData> = data
            .iter()
            .map(|data| (data.index() as usize, data.clone()))
            .collect();

        data = args
            .sections
            .iter()
            .map(|section| *section as usize)
            .filter_map(|idx| data_indexed.get(&idx))
            .map(Clone::clone)
            .collect();
    } else {
        data.sort_by_key(SectionData::index);
    }

    multi.clear().context("failed to clear MultiProgress")?;

    match args.format.unwrap_or_default() {
        Format::Raw => format::raw(&data, &args)?,
        #[cfg(feature = "fmt_bat")]
        Format::Bat => format::bat(&data, &args)?,
        #[cfg(feature = "fmt_json")]
        Format::Json => format::json(&data, &args)?,
        #[cfg(feature = "fmt_toml")]
        Format::Toml => format::toml(&data, &args)?,
    }

    Ok(())
}
