// SPDX-License-Identifier: MPL-2.0

mod general;
mod hardware;
mod network;
mod packages;
mod disk;

use indicatif::MultiProgress;
use serde::Serialize;
use std::fmt;

use crate::cli::Section;

pub trait GetData {
    fn get(progress_group: MultiProgress) -> anyhow::Result<Self>
    where
        Self: std::marker::Sized;
}

use general::GeneralData;
use hardware::HardwareData;
use network::NetworkData;
use packages::PackageDataVec;
use disk::DiskData;

#[allow(clippy::large_enum_variant)]
#[derive(Debug, Serialize, Clone)]
#[serde(untagged)]
pub enum SectionData {
    General(GeneralData),
    Hardware(HardwareData),
    Networks(NetworkData),
    Packages(PackageDataVec),
    Disks(DiskData),
}

impl SectionData {
    #[cfg(feature = "fmt_bat")]
    pub fn as_bytes(&self) -> Vec<u8> {
        self.to_string().into_bytes()
    }

    /// Defines opinionated order of sections, is overridden by `--sections|-s`
    pub const fn index(&self) -> u8 {
        match self {
            Self::General(_) => 0,
            Self::Hardware(_) => 4,
            Self::Networks(_) => 2,
            Self::Packages(_) => 1,
            Self::Disks(_) => 3,
        }
    }

    pub const fn title(&self) -> &str {
        match self {
            Self::General(_) => "General",
            Self::Hardware(_) => "Hardware",
            Self::Networks(_) => "Networks",
            Self::Packages(_) => "Packages",
            Self::Disks(_) => "Disks",
        }
    }

    #[cfg(any(feature = "fmt_json", feature = "fmt_toml",))]
    pub const fn name(&self) -> &str {
        match self {
            Self::General(_) => "general",
            Self::Hardware(_) => "hardware",
            Self::Networks(_) => "networks",
            Self::Packages(_) => "packages",
            Self::Disks(_) => "disks",
        }
    }

    pub fn get(section: Section, multi: MultiProgress) -> anyhow::Result<Self> {
        Ok(match section {
            Section::General => {
                let data = GeneralData::get(multi)?;

                Self::General(data)
            }
            Section::Hardware => {
                let data = HardwareData::get(multi)?;

                Self::Hardware(data)
            }
            Section::Networks => {
                let data = NetworkData::get(multi)?;

                Self::Networks(data)
            }
            Section::Packages => {
                let data = PackageDataVec::get(multi)?;

                Self::Packages(data)
            }
            Section::Disks => {
                let data = DiskData::get(multi)?;

                Self::Disks(data)
            }
        })
    }
}

impl fmt::Display for SectionData {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let str = match self {
            Self::General(data) => data.to_string(),
            Self::Hardware(data) => data.to_string(),
            Self::Networks(data) => data.to_string(),
            Self::Packages(data) => data.to_string(),
            Self::Disks(data) => data.to_string(),
        };

        write!(f, "{str}")
    }
}
