// SPDX-License-Identifier: MIT

use anyhow::Context;
use indicatif::{ProgressBar, ProgressStyle};
use serde::ser::{Serialize, SerializeSeq, Serializer};
use std::fmt;
use std::time::Duration;

use crate::INDENT;
use lib_sysinfo::{
    disk::{self, Fs, FsType},
    units::IecUnits,
    UnitSystem,
};

#[derive(Debug, Clone)]
pub struct DiskData(Vec<Fs>);

impl Serialize for DiskData {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut seq = serializer.serialize_seq(Some(self.0.len()))?;
        for element in &self.0 {
            seq.serialize_element(element)?;
        }
        seq.end()
    }
}

impl fmt::Display for DiskData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.0.is_empty() {
            writeln!(f, "No filesystems found")?;
        } else {
            writeln!(f, "Disk Usage: ")?;

            let filesystems: Vec<_> = self
                .0
                .iter()
                .filter(|fs| {
                    if fs.size.total.value() == 0f64 {
                        return false;
                    }

                    let used = fs.size.total - fs.size.available;

                    let is_usage_zero = used.value() == 0f64;
                    let is_fstype_tmpfs = fs.fs_type == FsType::Tmpfs;

                    !(is_usage_zero & !is_fstype_tmpfs)
                })
                .collect();

            for (idx, filesystem) in filesystems.iter().enumerate() {
                let print_spacer = idx < (filesystems.len() - 1);

                Self::fmt_fs(filesystem, print_spacer, f)?;
            }
        }

        Ok(())
    }
}

impl DiskData {
    fn fmt_fs(fs: &Fs, print_spacer: bool, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let Fs {
            device,
            mount_path,
            size,
            ..
        } = fs;

        let (total, total_units) = size.total.human::<IecUnits>();
        let (used, used_units) = {
            let used = size.total - size.available;

            used.human::<IecUnits>()
        };

        let percent = if used != 0f64 {
            let used = size.total - size.available;

            (used.value() / size.total.value()) * 100f64
        } else {
            0f64
        };

        writeln!(f, "{INDENT}{device} -> {mount_path}")?;

        writeln!(
            f,
            "{INDENT}Usage - {used:.2} {used_units} / {total:.2} {total_units} [{percent:.2}%]"
        )?;

        if print_spacer {
            writeln!(f)?;
        }

        Ok(())
    }
}

impl super::GetData for DiskData {
    fn get(progress_group: indicatif::MultiProgress) -> anyhow::Result<Self> {
        let total: u64 = 1;

        let pb = progress_group.add(ProgressBar::new(total));

        pb.enable_steady_tick(Duration::from_millis(100));

        pb.set_style(
            ProgressStyle::with_template("[{msg}] {wide_bar} [{pos}/{len}] [{elapsed}]")
                .context("invalid template")?
                .progress_chars("##-"),
        );

        pb.set_message("Getting Filesystems");

        let data: Vec<Fs> = disk::get_disk_usage().context("failed to get filesystems")?;

        let data: Vec<Fs> = data
            .iter()
            .filter(|fs| fs.fs_type != FsType::Unknown)
            .map(Clone::clone)
            .collect();

        pb.inc(1);

        pb.set_message("Filesystems: Done");

        pb.finish();

        Ok(Self(data))
    }
}
