// SPDX-License-Identifier: MPL-2.0

#![allow(clippy::similar_names)]

use anyhow::Context;
use indicatif::{MultiProgress, ProgressBar, ProgressStyle};
use serde::Serialize;
use std::fmt;
use std::sync::Arc;
use std::time::Duration;

use lib_sysinfo::hardware::{self, types};

use crate::INDENT;

#[derive(Debug, Serialize, Clone)]
pub struct HardwareData {
    pub host: types::HostInfo,
    pub bios: types::BiosInfo,
    pub cpus: Vec<types::CpuInfo>,
    pub cpu_arch: Arc<str>,
    pub gpus: Vec<types::GpuInfo>,
    pub audio_devices: Vec<types::AudioInfo>,
    pub network_devices: Vec<types::NetworkInfo>,
}

impl fmt::Display for HardwareData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.fmt_host(f)?;
        writeln!(f)?;

        self.fmt_bios(f)?;
        writeln!(f)?;

        self.fmt_cpu(f)?;
        writeln!(f)?;

        self.fmt_gpu(f)?;
        writeln!(f)?;

        self.fmt_audio(f)?;
        writeln!(f)?;

        self.fmt_network(f)
    }
}

impl HardwareData {
    fn fmt_host(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let types::HostInfo { vendor, family } = &self.host;

        writeln!(f, "Host:")?;
        writeln!(f, "{INDENT}Vendor - {vendor}")?;
        writeln!(f, "{INDENT}Family - {family}")
    }

    fn fmt_bios(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let types::BiosInfo {
            vendor,
            version,
            date,
            release,
        } = &self.bios;

        writeln!(f, "BIOS:")?;
        writeln!(f, "{INDENT}Vendor - {vendor}")?;
        writeln!(f, "{INDENT}Version - {version}")?;
        writeln!(f, "{INDENT}Date - {date}")?;
        writeln!(f, "{INDENT}Release - {release}")
    }

    fn fmt_cpu(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "CPU:")?;
        writeln!(f, "{INDENT}Architecture - {0}", self.cpu_arch)?;

        if self.cpus.len() > 1 {
            let _: Vec<_> = self
                .cpus
                .iter()
                .enumerate()
                .map(|(idx, cpu)| {
                    let types::CpuInfo {
                        model,
                        cores,
                        threads,
                    } = cpu;

                    writeln!(f, "{INDENT}{idx}:")?;
                    writeln!(f, "{INDENT}{INDENT}Model - {model}")?;
                    writeln!(f, "{INDENT}{INDENT}Cores - {cores}")?;
                    writeln!(f, "{INDENT}{INDENT}Threads - {threads}")?;

                    Ok(())
                })
                .collect::<Result<Vec<()>, fmt::Error>>()?;
        } else {
            let types::CpuInfo {
                model,
                cores,
                threads,
            } = &self.cpus[0];

            writeln!(f, "{INDENT}Model - {model}")?;
            writeln!(f, "{INDENT}Cores - {cores}")?;
            writeln!(f, "{INDENT}Threads - {threads}")?;
        }

        Ok(())
    }

    fn fmt_gpu(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "GPU:")?;

        if self.gpus.len() > 1 {
            let _: Vec<_> = self
                .gpus
                .iter()
                .enumerate()
                .map(|(idx, gpu)| {
                    let types::GpuInfo {
                        description,
                        product,
                        vendor,
                    } = gpu;

                    writeln!(f, "{INDENT}{idx}:")?;
                    writeln!(f, "{INDENT}{INDENT}Vendor - {vendor}")?;
                    writeln!(f, "{INDENT}{INDENT}Product - {product}")?;
                    writeln!(f, "{INDENT}{INDENT}Description - {description}")?;

                    Ok(())
                })
                .collect::<Result<Vec<()>, fmt::Error>>()?;
        } else {
            let types::GpuInfo {
                description,
                product,
                vendor,
            } = &self.gpus[0];

            writeln!(f, "{INDENT}Vendor - {vendor}")?;
            writeln!(f, "{INDENT}Product - {product}")?;
            writeln!(f, "{INDENT}Description - {description}")?;
        }

        Ok(())
    }

    fn fmt_audio(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "Audio Devices:")?;

        if self.audio_devices.len() > 1 {
            let _: Vec<_> = self
                .audio_devices
                .iter()
                .enumerate()
                .map(|(idx, device)| {
                    let types::AudioInfo { product, vendor } = device;

                    writeln!(f, "{INDENT}{idx}:")?;
                    writeln!(f, "{INDENT}{INDENT}Vendor - {vendor}")?;
                    writeln!(f, "{INDENT}{INDENT}Product - {product}")?;

                    Ok(())
                })
                .collect::<Result<Vec<()>, fmt::Error>>()?;
        } else {
            let types::AudioInfo { product, vendor } = &self.audio_devices[0];

            writeln!(f, "{INDENT}Vendor - {vendor}")?;
            writeln!(f, "{INDENT}Product - {product}")?;
        }

        Ok(())
    }

    fn fmt_network(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "Network Devices:")?;

        if self.network_devices.len() > 1 {
            let _: Vec<_> = self
                .network_devices
                .iter()
                .enumerate()
                .map(|(idx, device)| {
                    let types::NetworkInfo { product, vendor } = device;

                    writeln!(f, "{INDENT}{idx}:")?;
                    writeln!(f, "{INDENT}{INDENT}Vendor - {vendor}")?;
                    writeln!(f, "{INDENT}{INDENT}Product - {product}")?;

                    Ok(())
                })
                .collect::<Result<Vec<()>, fmt::Error>>()?;
        } else {
            let types::NetworkInfo { product, vendor } = &self.network_devices[0];

            writeln!(f, "{INDENT}Vendor - {vendor}")?;
            writeln!(f, "{INDENT}Product - {product}")?;
        }

        Ok(())
    }
}

impl super::GetData for HardwareData {
    fn get(progress_group: MultiProgress) -> anyhow::Result<Self> {
        let total: u64 = 7;

        let pb = progress_group.add(ProgressBar::new(total));

        pb.enable_steady_tick(Duration::from_millis(100));

        pb.set_style(
            ProgressStyle::with_template("[{msg}] {wide_bar} [{pos}/{len}] [{elapsed}]")
                .context("invalid template")?
                .progress_chars("##-"),
        );

        pb.set_message("Getting Host Info");

        let host = hardware::host_info().context("failed to get host info")?;

        pb.inc(1);

        pb.set_message("Getting BIOS Info");

        let bios = hardware::bios_info().context("failed to get bios info")?;

        pb.inc(1);

        pb.set_message("Getting CPU Info");

        let cpus: Vec<_> = hardware::cpu_info().context("failed to get cpu info")?;

        pb.inc(1);

        pb.set_message("Getting CPU Architecture");

        let cpu_arch: Arc<str> = hardware::cpu_arch()
            .context("failed to get cpu architecture")?
            .into();

        pb.inc(1);

        pb.set_message("Getting GPU Info");

        let gpus: Vec<_> = hardware::gpu_info().context("failed to get gpu info")?;

        pb.inc(1);

        pb.set_message("Getting Audio Info");

        let audio_devices: Vec<_> = hardware::audio_info().context("failed to get audio info")?;

        pb.inc(1);

        pb.set_message("Getting Network Info");

        let network_devices: Vec<_> =
            hardware::network_info().context("failed to get network info")?;

        pb.inc(1);

        let data = Self {
            host,
            bios,
            cpus,
            cpu_arch,
            gpus,
            audio_devices,
            network_devices,
        };

        pb.set_message("Hardware: Done");

        pb.finish();

        Ok(data)
    }
}
