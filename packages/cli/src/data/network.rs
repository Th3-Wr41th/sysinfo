// SPDX-License-Identifier: MPL-2.0

use anyhow::Context;
use indicatif::{MultiProgress, ProgressBar, ProgressStyle};
use serde::ser::{Serialize, SerializeSeq, Serializer};
use std::fmt;
use std::time::Duration;

use lib_sysinfo::network::{self, Ip};

use crate::INDENT;

#[derive(Debug, Clone)]
pub struct NetworkData(Vec<network::Interface>);

impl Serialize for NetworkData {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut seq = serializer.serialize_seq(Some(self.0.len()))?;
        for element in &self.0 {
            seq.serialize_element(element)?;
        }
        seq.end()
    }
}

impl fmt::Display for NetworkData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.0.is_empty() {
            writeln!(f, "No interfaces found")?;
        } else {
            for (idx, interface) in self.0.iter().enumerate() {
                Self::fmt_interface(interface, f)?;

                if idx < (self.0.len() - 1) {
                    writeln!(f)?;
                }
            }
        }
        Ok(())
    }
}

impl NetworkData {
    fn fmt_interface(interface: &network::Interface, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let network::Interface {
            name,
            ip_addresses,
            mac_address,
            net_type,
            state,
            ..
        } = interface;

        writeln!(f, "{name} [{state}]: ")?;

        Self::fmt_ip(ip_addresses, f)?;

        writeln!(f, "{INDENT}Mac Address: {mac_address}")?;

        writeln!(f, "{INDENT}Type: {net_type}")?;

        Ok(())
    }

    fn fmt_ip(ips: &[Ip], f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if !ips.is_empty() {
            writeln!(f, "{INDENT}Ip Addresses: ")?;

            for ip in ips {
                let ip_type = match ip {
                    Ip::V4 { .. } => "inet",
                    Ip::V6 { .. } => "inet6",
                };

                writeln!(f, "{INDENT}{INDENT}{ip_type} {ip}")?;
            }
        }

        Ok(())
    }
}

impl super::GetData for NetworkData {
    fn get(progress_group: MultiProgress) -> anyhow::Result<Self> {
        let total: u64 = 1;

        let pb = progress_group.add(ProgressBar::new(total));

        pb.enable_steady_tick(Duration::from_millis(100));

        pb.set_style(
            ProgressStyle::with_template("[{msg}] {wide_bar} [{pos}/{len}] [{elapsed}]")
                .context("invalid template")?
                .progress_chars("##-"),
        );

        pb.set_message("Getting Networks");

        let data: Vec<network::Interface> =
            network::get_network_info().context("failed to get networks")?;

        pb.inc(1);

        pb.set_message("Networks: Done");

        pb.finish();

        Ok(Self(data))
    }
}
