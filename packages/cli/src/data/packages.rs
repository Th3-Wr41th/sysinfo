// SPDX-License-Identifier: MPL-2.0

use anyhow::Context;
use indicatif::{MultiProgress, ProgressBar, ProgressStyle};
use serde::ser::{Serialize, SerializeSeq, Serializer};
use std::fmt;
use std::time::Duration;
use strum::IntoEnumIterator;

use lib_sysinfo::packages::PackageManager;

use crate::INDENT;

fn to_title_case(string: &str) -> String {
    string
        .split(' ')
        .map(|word| {
            word.chars()
                .take(1)
                .flat_map(char::to_uppercase)
                .chain(word.chars().skip(1))
                .collect::<String>()
        })
        .collect::<Vec<String>>()
        .join(" ")
}

#[derive(Debug, serde::Serialize, Clone)]
pub struct PackageData {
    manager: PackageManager,
    count: u64,
    updates: u64,
}

impl fmt::Display for PackageData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let Self {
            manager,
            count,
            updates,
        } = self;

        writeln!(f, "{0}", to_title_case(manager.name()))?;
        writeln!(f, "{INDENT}- {count} packages")?;
        writeln!(f, "{INDENT}~ {updates} updates")
    }
}

#[derive(Debug, Clone)]
pub struct PackageDataVec(Vec<PackageData>);

impl Serialize for PackageDataVec {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut seq = serializer.serialize_seq(Some(self.0.len()))?;
        for element in &self.0 {
            seq.serialize_element(element)?;
        }
        seq.end()
    }
}

impl fmt::Display for PackageDataVec {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let formatted = self
            .0
            .iter()
            .map(ToString::to_string)
            .collect::<Vec<_>>()
            .join("\n");

        write!(f, "{formatted}",)
    }
}

impl super::GetData for PackageDataVec {
    fn get(progress_group: MultiProgress) -> anyhow::Result<Self> {
        let total: u64 = PackageManager::iter()
            .count()
            .try_into()
            .context("failed to convert usize value to u64")?;

        let pb = progress_group.add(ProgressBar::new(total));

        pb.enable_steady_tick(Duration::from_millis(100));

        pb.set_style(
            ProgressStyle::with_template("[{msg}] {wide_bar} [{pos}/{len}] [{elapsed}]")
                .context("invalid template")?
                .progress_chars("##-"),
        );

        pb.set_message("Getting Packages");

        let iter_post1 = PackageManager::iter().filter_map(|manager| {
            pb.set_message("Counting Packages");

            let count = manager.count().ok()?;

            pb.inc(1);

            Some((manager, count))
        });

        pb.set_position(0);

        let data: Vec<(PackageManager, u64, u64)> = iter_post1
            .filter_map(|(manager, count)| {
                pb.set_message("Counting Updates");

                let updates = manager.updates().ok()?;

                pb.inc(1);

                Some((manager, count, updates))
            })
            .collect();

        let data_fmt: Vec<PackageData> = data
            .iter()
            .map(|(manager, count, updates)| PackageData {
                manager: *manager,
                count: *count,
                updates: *updates,
            })
            .collect();

        pb.set_message("Packages: Done");

        pb.finish();

        Ok(Self(data_fmt))
    }
}
