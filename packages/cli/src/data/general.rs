// SPDX-License-Identifier: MPL-2.0

use anyhow::Context;
use indicatif::{MultiProgress, ProgressBar, ProgressStyle};
use serde::Serialize;
use std::fmt;
use std::sync::Arc;
use std::time::Duration;

use lib_sysinfo::units::IecUnits;
use lib_sysinfo::general::{self, LoadAvg, MemInfo};
use lib_sysinfo::UnitSystem;

#[derive(Debug, Serialize, Clone)]
pub struct GeneralData {
    kernel: Arc<str>,
    distribution: Arc<str>,
    uptime: Arc<str>,
    loadavg: LoadAvg,
    processes: u64,
    meminfo: MemInfo,
}

impl fmt::Display for GeneralData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "Kernel - {0}", self.kernel.clone())?;
        writeln!(f)?;

        writeln!(f, "Distribution - {0}", self.distribution.clone())?;
        writeln!(f)?;

        writeln!(f, "Uptime - {0}", self.uptime.clone())?;
        writeln!(f)?;

        writeln!(f, "Load Averages - {0}", self.loadavg.clone())?;
        writeln!(f)?;

        writeln!(f, "Processes - {0}", self.processes)?;
        writeln!(f)?;

        let (mem_total, mem_unit_total) = self.meminfo.total.human::<IecUnits>();

        let (mem_used, mem_unit_used) =
            (self.meminfo.total - self.meminfo.available).human::<IecUnits>();

        writeln!(
            f,
            "Memory - {0:.2} {2} / {1:.2} {3}",
            mem_used, mem_total, mem_unit_used, mem_unit_total
        )?;
        writeln!(f)?;

        let (swap_total, swap_unit_total) = self.meminfo.total.human::<IecUnits>();

        let (swap_used, swap_unit_used) =
            (self.meminfo.swap.total - self.meminfo.swap.free).human::<IecUnits>();

        writeln!(
            f,
            "Swap - {0:.2} {2} / {1:.2} {3}",
            swap_used, swap_total, swap_unit_used, swap_unit_total
        )
    }
}

impl super::GetData for GeneralData {
    fn get(progress_group: MultiProgress) -> anyhow::Result<Self> {
        let total: u64 = 6;

        let pb = progress_group.add(ProgressBar::new(total));

        pb.enable_steady_tick(Duration::from_millis(100));

        pb.set_style(
            ProgressStyle::with_template("[{msg}] {wide_bar} [{pos}/{len}] [{elapsed}]")
                .context("invalid template")?
                .progress_chars("##-"),
        );

        pb.set_message("Getting Kernel");

        let kernel = general::kernel().context("failed to get kernel")?;

        pb.inc(1);

        pb.set_message("Getting distribution");

        let distribution = general::distribution().context("failed to get distribution")?;

        pb.inc(1);

        pb.set_message("Getting uptime");

        let uptime = general::uptime().context("failed to get uptime")?;

        pb.inc(1);

        pb.set_message("Getting load averages");

        let loadavg = general::loadavg().context("failed to get load averages")?;

        pb.inc(1);

        pb.set_message("Getting processes");

        let processes = general::processes().context("failed to get processes")?;

        pb.inc(1);

        pb.set_message("Getting memory usage information");

        let meminfo = general::meminfo().context("failed to get memory usage information")?;

        pb.inc(1);

        let data = Self {
            kernel,
            distribution,
            uptime,
            loadavg,
            processes,
            meminfo,
        };

        pb.set_message("General: Done");

        pb.finish();

        Ok(data)
    }
}
