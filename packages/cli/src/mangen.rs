// SPDX-License-Identifier: MPL-2.0

#![cfg(feature = "mangen")]

use clap::CommandFactory;
use std::path::PathBuf;
use std::{env, fs, io};

use crate::cli::Args;

pub fn mangen() -> io::Result<()> {
    let cmd = Args::command().mut_arg("mangen", |arg| arg.hide(true));

    let out_dir = env::var("CARGO_MANIFEST_DIR")
        .map_err(|err| io::Error::new(io::ErrorKind::NotFound, err))?;
    let out_file =
        env::var("CARGO_PKG_NAME").map_err(|err| io::Error::new(io::ErrorKind::NotFound, err))?;

    let out_dir_path = PathBuf::from(out_dir).join("man");

    if !out_dir_path.exists() {
        fs::create_dir_all(&out_dir_path)?;
    }

    let out_file_path = PathBuf::from(out_file).with_extension("1");

    let man = clap_mangen::Man::new(cmd);
    let mut buffer: Vec<u8> = Vec::new();

    man.render(&mut buffer)?;

    let path = out_dir_path.join(out_file_path);

    fs::write(&path, buffer)?;

    println!("Wrote man file to: {0}", path.to_string_lossy());

    Ok(())
}
