// SPDX-License-Identifier: MIT

use lib_sysinfo::packages::managers::{PackageManager, Rpm};

fn main() {
    // Count the number of rpm packages
    let res = Rpm.count();

    match res {
        Ok(count) => println!("There are {count} rpm packages installed on this system"),
        Err(msg) => {
            println!("Could not get the number of rpm packages installed on this system. {msg}")
        }
    }
}
