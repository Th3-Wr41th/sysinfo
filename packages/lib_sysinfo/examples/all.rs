// SPDX-License-Identifier: MIT

use lib_sysinfo::general;
use lib_sysinfo::hardware::*;
use lib_sysinfo::network::*;

fn main() {
    let distribution = general::distribution().unwrap();
    let uptime = general::uptime().unwrap();

    let cpus: Vec<_> = cpu_info().unwrap();

    let cpus_formatted: Vec<_> = cpus.iter().map(|cpu| cpu.model.as_ref()).collect();

    let interfaces: Vec<Interface> = get_network_info().unwrap();

    let interfaces_formatted: Vec<_> = interfaces
        .iter()
        .map(|interface| format!("{0}: {1:#?}", interface.name, interface.ip_addresses))
        .collect();

    println!("Uptime: {uptime}\n");

    println!("Distribution: {distribution}\n");

    println!(
        "CPUs: \n{0}{1}\n",
        " ".repeat(2),
        cpus_formatted.join("\n  ")
    );

    println!(
        "Interfaces: \n{0}{1}",
        " ".repeat(2),
        interfaces_formatted.join("\n  ")
    );
}
