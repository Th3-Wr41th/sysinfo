// SPDX-License-Identifier: MIT

use lib_sysinfo::general;

fn main() {
    // Get data
    let kernel = general::kernel().unwrap();
    let distribution = general::distribution().unwrap();
    let uptime = general::uptime().unwrap();
    let loadavg = general::loadavg().unwrap();
    let meminfo = general::meminfo().unwrap();

    println!("Kernel: {kernel}");

    println!("Distribution: {distribution}");

    println!("Uptime: {uptime}");

    println!(
        "Load Average: {0} {1} {2}",
        loadavg.one_min, loadavg.five_min, loadavg.fifteen_min
    );

    println!(
        "Memory: {0}/{1}",
        (meminfo.total - meminfo.available),
        meminfo.total
    );

    println!(
        "Swap: {0}/{1}",
        (meminfo.swap.total - meminfo.swap.free),
        meminfo.swap.total
    )
}
