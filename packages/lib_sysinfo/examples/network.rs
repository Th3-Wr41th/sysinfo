// SPDX-License-Identifier: MIT

use lib_sysinfo::network::{get_network_info, Interface};

fn main() {
    let interfaces: Vec<Interface> = get_network_info().unwrap();

    for interface in &interfaces {
        println!("{0}: {1:#?}", interface.name, interface.ip_addresses);
    }
}
