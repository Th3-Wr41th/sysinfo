// SPDX-License-Identifier: MIT

use lib_sysinfo::hardware::*;

fn main() {
    let bios = bios_info().unwrap();
    let cpus: Vec<_> = cpu_info().unwrap();

    let cpus_formatted: Vec<_> = cpus.iter().map(|cpu| cpu.model.as_ref()).collect();

    println!("Bios: {0}-{1}", bios.vendor, bios.version,);

    println!("CPUs: \n{0}{1}", " ".repeat(2), cpus_formatted.join("\n  "));
}
