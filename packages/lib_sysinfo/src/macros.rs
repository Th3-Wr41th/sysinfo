// SPDX-License-Identifier: MIT

#[cfg(test)]
macro_rules! mockfs {
    ($($path:expr),+) => {
        include_str!(mock_path!($($path),+))
    };
}

#[cfg(test)]
macro_rules! mock_path {
    ($($path:expr),+) => {
        concat!(mock_path!(), $($path),+)
    };

    () => {
        concat!(env!("CARGO_MANIFEST_DIR"), "/mockfs/")
    };
}

#[cfg(test)]
macro_rules! mock_dir {
    ($($path:expr),+) => {{
        let dir = mock_path!($($path),+);

        if !std::path::Path::new(dir).exists() {
            std::fs::create_dir_all(dir).expect("failed to create mock dir");
        }

        dir
    }};
}

macro_rules! dir {
    ($root:expr, $path:expr) => {{
        #[cfg(test)]
        let root = mock_dir!(stringify!($root));

        #[cfg(not(test))]
        let root = $root;

        format!("{root}/{}", $path)
    }};
}

macro_rules! string {
    ($str_root: expr $(, $str: expr)*) => {{
        #[allow(unused_mut)]
        let mut s = String::from($str_root.to_string());

        $(s.push_str($str);)*

        s
    }};
}

macro_rules! read {
    ($test:expr, $file_root:expr $(, $file_part:expr)*) => {{
        #[cfg(test)]
        let res: crate::InternalResult<std::sync::Arc<str>> = {
            if stringify!($test) == stringify!("nonexistant") {
                Err(crate::InternalError::Io(
                    std::io::Error::new(
                        std::io::ErrorKind::NotFound,
                        "file is nonexistant",
                    ),
                    string!($test),
                ))
            } else {
                crate::read_file(format!("{0}/{1}", mock_path!(), $test))
            }
        };

        #[cfg(not(test))]
        let res: crate::InternalResult<std::sync::Arc<str>> = {
            #[allow(unused_mut)]
            let mut path = std::path::PathBuf::from($file_root);

            $(path.push($file_part);)*

            crate::read_file(path)
        };

        if let Err(err) = res {
            Err(err)
        } else {
            let content = res.unwrap();

            if content.is_empty() {
                Err(crate::InternalError::Io(
                    std::io::Error::new(
                    std::io::ErrorKind::InvalidData,
                        format!("No data found in `{{$file}}`"),
                    ),
                    string!($file_root $(, $file_part)*),
                ))
            } else {
                Ok(content.as_ref().trim().into())
            }
        }
    }}
}

macro_rules! read_dir {
    ($file: expr) => {{
        #[cfg(test)]
        let dir = crate::read_dir(mock_dir!(stringify!($file)));

        #[cfg(not(test))]
        let dir = crate::read_dir($file);

        dir
    }};
}

#[cfg(test)]
macro_rules! ron {
    ($file:expr) => {{
        let content = mockfs!($file);

        ron::from_str(content)
    }};
}
