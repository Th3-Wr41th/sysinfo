// SPDX-License-Identifier: MIT

use serde::{Deserialize, Serialize};
use unit_system::{Unit, UnitSystem};

#[derive(Debug, UnitSystem, Copy, Clone, PartialEq, Deserialize, Serialize)]
#[unit_system(add, sub)]
pub struct Byte(f64);

impl std::fmt::Display for Byte {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let (val, unit) = self.human::<IecUnits>();

        write!(f, "{val:.2} {unit}")
    }
}

#[derive(Unit, Copy, Clone, Deserialize, Serialize)]
#[unit(base = 1024)]
pub enum IecUnits {
    /// `Bytes`
    B,
    /// `Kibibytes`
    KiB,
    /// `Mebibytes`
    MiB,
    /// `Gibibytes`
    GiB,
    /// `Tebibytes`
    TiB,
    /// `Pebibytes`
    PiB,
}

impl std::fmt::Display for IecUnits {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{0}", self.unit())
    }
}
