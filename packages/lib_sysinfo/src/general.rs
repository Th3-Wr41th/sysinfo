// SPDX-License-Identifier: MIT

//! Get information regarding general system information
//!
//! # Examples
//!
//! ```
//! # use lib_sysinfo::general;
//! // Get data
//! let kernel = general::kernel().unwrap();
//! let distribution = general::distribution().unwrap();
//! let uptime = general::uptime().unwrap();
//!
//! println!("Kernel: {kernel}");
//!
//! println!("Distribution: {distribution}");
//!
//! println!("Uptime: {uptime}");
//! ```


use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::env::consts::OS;
use std::io::{Error, ErrorKind};
use std::sync::Arc;
use std::time::{Duration, SystemTime, SystemTimeError, UNIX_EPOCH};
use std::{fmt, num};

use crate::units::{Byte, IecUnits};
use crate::{InternalError, InternalResult};
use unit_system::UnitSystem;

const KERNEL_OSRELEASE: &str = "/proc/sys/kernel/osrelease";
const OSRELEASE: &str = "/etc/os-release";
const STAT: &str = "/proc/stat";
const LOADAVG: &str = "/proc/loadavg";
const PROC: &str = "/proc";
const MEMINFO: &str = "/proc/meminfo";

/// Reads the current kernel version
///
/// # Errors
/// Will return an error if it cannot read `/proc/sys/kernel/osrelease`
pub fn kernel() -> InternalResult<Arc<str>> {
    read!("KERNEL_OSRELEASE/input", KERNEL_OSRELEASE)
}

/// Reads the [`PRETTY_NAME`](https://www.freedesktop.org/software/systemd/man/latest/os-release.html)
/// from `/etc/os-release`
///
/// # Errors
///
/// Will return an error if it cannot read `/etc/os-release` or cannot find the key
/// [`PRETTY_NAME`](https://www.freedesktop.org/software/systemd/man/latest/os-release.html)
/// in `/etc/os-release`
pub fn distribution() -> InternalResult<Arc<str>> {
    fn try_read() -> InternalResult<Arc<str>> {
        let content: Arc<str> = read!("OSRELEASE/input", OSRELEASE)?;

        let names: Vec<&str> = content
            .lines()
            .filter_map(|line| {
                let Some((key, val)) = line.split_once('=') else {
                    return None;
                };

                if key == "PRETTY_NAME" {
                    Some(val)
                } else {
                    None
                }
            })
            .collect();

        let name_option: Option<Arc<str>> = names
            .iter()
            .map(|name| name.trim().trim_matches('"').into())
            .next();

        name_option.map_or_else(
            || {
                Err(InternalError::Io(
                    Error::new(
                        ErrorKind::InvalidData,
                        format!("No key `PRETTY_NAME` found in `{OSRELEASE}`"),
                    ),
                    string!(OSRELEASE),
                ))
            },
            Ok,
        )
    }

    match try_read() {
        Ok(val) => Ok(val),
        Err(err) => {
            if OS.is_empty() {
                Err(err)
            } else {
                Ok(OS.into())
            }
        }
    }
}

/// Reads the boot time (in seconds since the Epoch) from `/proc/stat`
///
/// # Errors
///
/// Will return an error if it cannot read `/proc/stat` or cannot find the key `btime` in `/proc/stat`
pub fn btime() -> InternalResult<u64> {
    let content: Arc<str> = read!("STAT/input", STAT)?;

    let btimes: Vec<_> = content
        .lines()
        .filter_map(|line| {
            let Some((key, value)) = line.split_once(' ') else {
                return None;
            };

            if key == "btime" {
                Some(value)
            } else {
                None
            }
        })
        .collect();

    btimes.first().map_or_else(
        || {
            Err(InternalError::Io(
                Error::new(ErrorKind::InvalidData, "No key `btime` found in `{STAT}`"),
                string!(STAT),
            ))
        },
        |btime| {
            btime.parse().map_err(|err: std::num::ParseIntError| {
                InternalError::Unknown(err.into(), string!("btime is not a vaild number"))
            })
        },
    )
}

/// Formats boot time into the format `hh:mm:ss`
///
/// # Errors
///
/// Will return an err if [`btime`] returns an error or if the system time is before the Epoch
pub fn uptime() -> InternalResult<Arc<str>> {
    let boot_time: u64 = btime()?;

    let now = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .map_err(|err: SystemTimeError| {
            InternalError::Unknown(err.into(), string!("Invalid time"))
        })?;

    let duration = now - Duration::from_secs(boot_time);

    let time_secs = duration.as_secs();

    let (hours, remaining) = (time_secs / 3600, time_secs % 3600);
    let (mins, secs) = (remaining / 60, remaining % 60);
    let hhmmss = format!("{hours:02}:{mins:02}:{secs:02}");

    Ok(hhmmss.into())
}

#[derive(Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
pub struct LoadAvg {
    pub one_min: Arc<str>,
    pub five_min: Arc<str>,
    pub fifteen_min: Arc<str>,
}

impl fmt::Display for LoadAvg {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{0} {1} {2}",
            self.one_min, self.five_min, self.fifteen_min
        )
    }
}

/// Read the load averages from `/proc/loadavg`
///
/// # Errors
///
/// Will return an error if it cannot read `/proc/loadavg` or if the file does contain the correct
/// amount of data
pub fn loadavg() -> InternalResult<LoadAvg> {
    let content: Arc<str> = read!("LOADAVG/input", LOADAVG)?;

    let values: Vec<_> = content.split(' ').collect();

    if values.len() != 5 {
        return Err(InternalError::Io(
            Error::new(
                ErrorKind::InvalidData,
                format!("{LOADAVG} did not the correct amount of data"),
            ),
            string!(LOADAVG),
        ));
    }

    let one_min: Arc<str> = values[0].into();
    let five_min: Arc<str> = values[1].into();
    let fifteen_min: Arc<str> = values[2].into();

    let avg = LoadAvg {
        one_min,
        five_min,
        fifteen_min,
    };

    Ok(avg)
}

/// Get the number of running processes
///
/// # Errors
///
/// Will return an error if it cannot read the `/proc` directory or if the count cannot be parsed
/// into a u64 type
pub fn processes() -> InternalResult<u64> {
    let proc_dir = read_dir!(PROC)?;

    let count: u64 = proc_dir
        .filter_map(|dir| {
            let Ok(dir) = dir else { return None };

            let name = dir.file_name();

            if name.to_string_lossy().parse::<u64>().is_ok() {
                Some(name)
            } else {
                None
            }
        })
        .count()
        .try_into()
        .map_err(|err: num::TryFromIntError| InternalError::TryFromIntError(err))?;

    Ok(count)
}

#[derive(Debug, PartialEq, Copy, Clone, Serialize, Deserialize)]
pub struct MemInfo {
    pub total: Byte,
    pub available: Byte,
    pub free: Byte,
    pub swap: Swap,
}

#[derive(Debug, PartialEq, Copy, Clone, Serialize, Deserialize)]
pub struct Swap {
    pub total: Byte,
    pub free: Byte,
}

pub fn meminfo() -> InternalResult<MemInfo> {
    macro_rules! err {
        ($key:expr) => {
            InternalError::Io(
                Error::new(
                    ErrorKind::InvalidData,
                    format!("{MEMINFO} does not contain {0}", $key),
                ),
                string!(MEMINFO),
            )
        };
    }

    let content: Arc<str> = read!("MEMINFO/input", MEMINFO)?;

    let data: HashMap<&str, Byte> = content
        .lines()
        .filter_map(|line| {
            let (key, val) = match line.split_once(':') {
                Some((key, val)) => (key.trim(), val.trim()),
                None => return None,
            };

            let val = match val.split_once(' ') {
                Some((val, _)) => val.trim(),
                None => return None,
            };

            let val: f64 = match val.parse() {
                Ok(val) => val,
                Err(_) => return None,
            };

            let byte_val: Byte = Byte::from_unit(val, IecUnits::KiB);

            Some((key, byte_val))
        })
        .collect();

    Ok(MemInfo {
        total: *data.get("MemTotal").ok_or(err!("MemTotal"))?,
        available: *data.get("MemAvailable").ok_or(err!("MemAvailable"))?,
        free: *data.get("MemFree").ok_or(err!("MemFree"))?,
        swap: Swap {
            total: *data.get("SwapTotal").ok_or(err!("SwapTotal"))?,
            free: *data.get("SwapFree").ok_or(err!("SwapFree"))?,
        },
    })
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_distribution() {
        let expected: &str = mockfs!("OSRELEASE/expected");

        let result: InternalResult<_> = distribution();

        assert!(result.is_ok());

        assert_eq!(result.unwrap(), expected.trim().into());
    }

    #[test]
    fn test_btime() {
        let expected: &str = mockfs!("STAT/expected");

        let result: InternalResult<_> = btime();

        assert!(result.is_ok());

        let result: u64 = result.unwrap();

        assert_eq!(format!("{result}"), expected.trim());
    }

    #[test]
    fn test_loadavg() {
        let expected: LoadAvg =
            ron!("LOADAVG/expected.ron").expect("failed to parse expected data");

        let result = loadavg();

        assert!(result.is_ok());

        let result = result.unwrap();

        assert_eq!(result, expected);
    }

    #[test]
    fn test_processes() {
        let expected: u64 = 3;

        let result = processes();

        assert!(result.is_ok());

        let result = result.unwrap();

        assert_eq!(result, expected);
    }

    #[test]
    fn test_meminfo() {
        let expected: MemInfo =
            ron!("MEMINFO/expected.ron").expect("failed to parse expected data");

        let result = meminfo();

        assert!(result.is_ok());

        let result = result.unwrap();

        assert_eq!(result, expected);
    }
}
