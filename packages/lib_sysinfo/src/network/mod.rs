// SPDX-License-Identifier: MIT

//! Get information regarding network interfaces
//!
//! # Example
//!
//! ```
//! # use lib_sysinfo::network::*;
//! let interfaces: Vec<Interface> = get_network_info().unwrap();
//!
//! for interface in &interfaces {
//!     println!("{0}: {1:#?}", interface.name, interface.ip_addresses);
//! }
//! ```

mod iface;
mod interface;
mod ip;

use std::collections::HashMap;
use std::sync::Arc;

use crate::{InternalError, InternalResult};

#[doc(inline)]
pub use ip::Ip;

#[doc(inline)]
pub use interface::Interface;

const NET: &str = "/sys/class/net";

use iface::IFace;

/// Get a list of all network interfaces on this machine
///
/// # Errors
///
/// Will return an error if it cannot get the interface addresses
pub fn get_network_info() -> InternalResult<Vec<Interface>> {
    let ifaces: Vec<IFace> = IFace::get_all()
        .map_err(|err| InternalError::Io(err, string!("failed to get interface addresses")))?;

    let mut interfaces: Vec<Interface> = {
        let mut map: HashMap<Arc<str>, Vec<Ip>> = HashMap::new();

        let _: Vec<_> = ifaces
            .iter()
            .map(|iface| {
                if let Some(vec) = map.get_mut(&iface.name) {
                    vec.push(iface.ip);
                } else {
                    map.insert(iface.name.clone(), vec![iface.ip]);
                }
            })
            .collect();

        let net_dir = read_dir!(NET)?;

        let _: Vec<_> = net_dir
            .filter_map(|dir| {
                let Ok(dir) = dir else { return None };

                let Ok(file_type) = dir.file_type() else {
                    return None;
                };

                #[cfg(test)]
                if !file_type.is_dir() {
                    return None;
                }

                #[cfg(not(test))]
                if !file_type.is_symlink() {
                    return None;
                }

                let name = dir.file_name();

                Some(name)
            })
            .map(|iface| {
                let name: Arc<str> = iface.to_string_lossy().into();

                if map.get(&name).is_none() {
                    map.insert(name, Vec::new());
                }
            })
            .collect();

        map.iter()
            .map(|(name, ips)| Interface::from_name(name, ips))
            .collect()
    };

    interfaces.sort_by_key(|interface| interface.index);

    Ok(interfaces)
}

#[cfg(test)]
mod tests {
    use super::*;

    use std::collections::HashSet;

    #[test]
    fn test_get_network_info() {
        let expected: Vec<Interface> = ron!("NET/expected").expect("failed to parse expected data");

        let result = get_network_info();

        assert!(result.is_ok());

        let result = result.unwrap();

        dbg!(&expected, &result);

        assert_eq!(
            result.iter().collect::<HashSet<_>>(),
            expected.iter().collect::<HashSet<_>>()
        );
    }
}
