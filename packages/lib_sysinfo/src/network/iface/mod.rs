// SPDX-License-Identifier: MIT

mod ifaddrs;
mod sockaddr;

use serde::{Deserialize, Serialize};
use std::io;
use std::sync::Arc;

use super::Ip;

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq, Hash)]
pub struct IFace {
    pub name: Arc<str>,
    pub ip: Ip,
}

impl IFace {
    /// Get one interface by name
    #[allow(dead_code)]
    pub fn get(name: &str) -> io::Result<Self> {
        let all: Vec<Self> = Self::get_all()?;

        let matched: Vec<_> = all
            .iter()
            .filter(|iface| iface.name == name.into())
            .collect();

        if matched.len() > 1 {
            Err(io::Error::new(
                io::ErrorKind::Other,
                "found more than one interface with that name",
            ))
        } else if matched.is_empty() {
            Err(io::Error::new(
                io::ErrorKind::Other,
                "no interfaces found with that name",
            ))
        } else {
            Ok(matched[0].clone())
        }
    }

    /// Get all interfaces on the system
    #[cfg(not(test))]
    pub fn get_all() -> io::Result<Vec<Self>> {
        use ifaddrs::IfAddrs;
        use std::ffi::CStr;
        use std::net::{IpAddr, Ipv4Addr, Ipv6Addr};

        let ifaddrs = IfAddrs::new()?;

        let ret: Vec<Self> = ifaddrs
            .iter()
            .filter_map(|ifaddr| {
                let ip: Ip = match sockaddr::to_ipaddr(ifaddr.ifa_addr) {
                    None => return None,
                    Some(IpAddr::V4(ipv4_addr)) => {
                        let netmask: Ipv4Addr = match sockaddr::to_ipaddr(ifaddr.ifa_netmask) {
                            Some(IpAddr::V4(netmask)) => netmask,
                            _ => Ipv4Addr::new(0, 0, 0, 0),
                        };

                        let count: u32 = netmask
                            .octets()
                            .iter()
                            .fold(0u32, |acc, octet| acc + octet.count_ones());

                        Ip::V4 {
                            ip: ipv4_addr,
                            cidr: count,
                        }
                    }
                    Some(IpAddr::V6(ipv6_addr)) => {
                        let netmask: Ipv6Addr = match sockaddr::to_ipaddr(ifaddr.ifa_netmask) {
                            Some(IpAddr::V6(netmask)) => netmask,
                            _ => Ipv6Addr::new(0, 0, 0, 0, 0, 0, 0, 0),
                        };

                        let count: u32 = netmask
                            .octets()
                            .iter()
                            .fold(0u32, |acc, octet| acc + octet.count_ones());

                        Ip::V6 {
                            ip: ipv6_addr,
                            prefix: count,
                        }
                    }
                };

                let name: Arc<str> = unsafe { CStr::from_ptr(ifaddr.ifa_name) }
                    .to_string_lossy()
                    .into();

                Some(IFace { name, ip })
            })
            .collect();

        Ok(ret)
    }

    /// Get all interfaces on the system
    #[cfg(test)]
    pub fn get_all() -> io::Result<Vec<Self>> {
        Ok(ron!("NET/interfaces.ron").expect("failed to parse `NET/interfaces.ron`"))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_iface() {
        let expected: IFace = IFace {
            name: "enp3s0".into(),
            ip: Ip::V4 {
                ip: "10.0.0.2".parse().expect("failed to parse ipv4 address"),
                cidr: 24,
            },
        };

        let result = IFace::get(&expected.name);

        assert!(result.is_ok());

        let result = result.unwrap();

        assert_eq!(result, expected);
    }
}
