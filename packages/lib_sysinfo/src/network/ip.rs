// SPDX-License-Identifier: MIT

use serde::{Deserialize, Serialize};
use std::fmt;
use std::net::{Ipv4Addr, Ipv6Addr};

/// A representation of an IP address with its netmask
#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Clone, Copy, Hash)]
#[serde(tag = "type")]
pub enum Ip {
    #[serde(rename = "ipv4")]
    V4 { ip: Ipv4Addr, cidr: u32 },
    #[serde(rename = "ipv6")]
    V6 { ip: Ipv6Addr, prefix: u32 },
}

impl fmt::Display for Ip {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::V4 { ip, cidr } => write!(f, "{ip}/{cidr}"),
            Self::V6 { ip, prefix } => write!(f, "{ip}/{prefix}"),
        }
    }
}
