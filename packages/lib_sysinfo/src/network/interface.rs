// SPDX-License-Identifier: MIT

use serde::{Deserialize, Serialize};
use std::fmt;
use std::path::Path;
use std::sync::Arc;

use super::{Ip, NET};

const NET_VLAN: &str = "/proc/net/vlan";
const NET_VIRTUAL: &str = "/sys/devices/virtual/net";

/// A representation of an interface
#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Clone, Hash)]
pub struct Interface {
    pub index: u32,
    pub name: Arc<str>,
    pub ip_addresses: Vec<Ip>,
    pub mac_address: Arc<str>,
    pub net_type: Arc<str>,
    pub is_virtual: bool,
    pub state: State,
}

#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Clone, Hash)]
pub enum State {
    Up,
    Down,
    Unknown,
}

impl fmt::Display for State {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Up => write!(f, "up"),
            Self::Down => write!(f, "down"),
            Self::Unknown => write!(f, "unknown"),
        }
    }
}

impl Interface {
    pub(super) fn from_name(name: &str, ip_addresses: &[Ip]) -> Self {
        let mac_path = format!("{name}/address");
        let mac: Arc<str> =
            read!(format_args!("NET/{mac_path}"), NET, &mac_path).map_or_else(|_| "00:00:00:00:00:00".into(), |val| val);

        let net_type = {
            let type_path = format!("{name}/type");
            let type_raw: Arc<str> = read!(format_args!("NET/{type_path}"), NET, &type_path).map_or_else(|_| "0".into(), |val| val);

            match type_raw.as_ref() {
                "1" => {
                    let test_paths = [
                        (dir!(NET, format_args!("{0}/wireless", name)), "wireless"),
                        (dir!(NET, format_args!("{0}/phy80211", name)), "wireless"),
                        (dir!(NET, format_args!("{0}/bridge", name)), "bridge"),
                        (dir!(NET_VLAN, format_args!("{0}", name)), "vlan"),
                        (dir!(NET, format_args!("{0}/bonding", name)), "bond"),
                        (dir!(NET, format_args!("{0}/tun_flags", name)), "tap"),
                    ];

                    let mut net_type = "ethernet";

                    for (path, value) in &test_paths {
                        if Path::new(&path).exists() {
                            net_type = value;
                        }
                    }

                    net_type
                }
                "24" => "ethernet",
                "32" => {
                    let test_paths = [
                        (dir!(NET, format_args!("{0}/bonding", name)), "bond"),
                        (
                            dir!(NET, format_args!("{0}/create_child", name)),
                            "infiniband",
                        ),
                    ];

                    let mut net_type = "infiniband-child";

                    for (path, value) in &test_paths {
                        if Path::new(&path).exists() {
                            net_type = value;
                        }
                    }

                    net_type
                }
                "512" => "point-to-point",
                "772" => "loopback",
                _ => "unknown",
            }
        };

        let is_virtual = {
            let path = dir!(NET_VIRTUAL, name);

            Path::new(&path).exists()
        };

        let index_path = format!("{name}/ifindex");
        let index_str: Arc<str> = read!(format_args!("NET/{index_path}"), NET, &index_path).map_or_else(|_| "0".into(), |val| val);

        let index: u32 = index_str.parse().expect("unable to parse ifindex value");

        let state_path = format!("{name}/operstate");
        let state_str: Arc<str> =
            read!(format_args!("NET/{state_path}"), NET, &state_path).map_or_else(|_| "unknown".into(), |val| val);

        let state = match state_str.as_ref() {
            "up" => State::Up,
            "down" => State::Down,
            _ => State::Unknown,
        };

        Self {
            index,
            name: name.into(),
            ip_addresses: ip_addresses.to_vec(),
            mac_address: mac,
            net_type: net_type.into(),
            is_virtual,
            state,
        }
    }
}
