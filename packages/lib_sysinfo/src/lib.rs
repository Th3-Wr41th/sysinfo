// SPDX-License-Identifier: MIT

//! Library to gather information about the system
//!
//! # Example
//!
//! ```
//! # use lib_sysinfo::general;
//! # use lib_sysinfo::hardware::*;
//! # use lib_sysinfo::network::*;
//! let distribution = general::distribution().unwrap();
//! let uptime = general::uptime().unwrap();
//!
//! let cpus: Vec<_> = cpu_info().unwrap();
//!
//! let cpus_formatted: Vec<_> = cpus.iter().map(|cpu| cpu.model.as_ref()).collect();
//!
//! let interfaces: Vec<Interface> = get_network_info().unwrap();
//!
//! let interfaces_formatted: Vec<_> = interfaces
//!     .iter()
//!     .map(|interface| format!("{0}: {1:#?}", interface.name, interface.ip_addresses))
//!     .collect();
//!
//! println!("Uptime: {uptime}\n");
//!
//! println!("Distribution: {distribution}\n");
//!
//! println!(
//!     "CPUs: \n{0}{1}\n",
//!     " ".repeat(2),
//!     cpus_formatted.join("\n  ")
//! );
//!
//! println!(
//!     "Interfaces: \n{0}{1}",
//!     " ".repeat(2),
//!     interfaces_formatted.join("\n  ")
//! );
//! ```

#[macro_use]
mod macros;

#[cfg(feature = "general")]
pub mod general;
#[cfg(feature = "hardware")]
pub mod hardware;
#[cfg(feature = "network")]
pub mod network;
#[cfg(feature = "packages")]
pub mod packages;
#[cfg(feature = "disk")]
pub mod disk;
pub mod units;

use std::path::Path;
use std::sync::Arc;
use std::{env, fmt, fs, io, num};

pub use unit_system::{Unit, UnitSystem};

type InternalResult<T> = Result<T, InternalError>;

#[derive(Debug)]
pub enum InternalError {
    Io(io::Error, String),
    Packages(io::Error),
    PackageCount(String),
    TryFromIntError(num::TryFromIntError),
    // Env(err, var)
    Env(env::VarError, String),
    // Command(err, bin)
    Command(String, String),
    // Unknown(err, msg)
    Unknown(Box<dyn std::error::Error + Send + Sync>, String),
}

impl fmt::Display for InternalError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let msg = match self {
            Self::Io(err, what) => format!("[Internal] An I/O error occured: {err} ({what})"),
            Self::Packages(err) => format!("[Internal] An error occured getting packages: {err}"),
            Self::PackageCount(msg) => {
                format!("[Internal] An error occured getting packages: {msg}")
            }
            err => format!("[Internal] An internal error occured: {err:?}"),
        };

        write!(f, "{msg}")
    }
}

impl std::error::Error for InternalError {}

fn read_file<P: AsRef<Path>>(path: P) -> InternalResult<Arc<str>> {
    fs::read_to_string(&path)
        .map_err(|err| InternalError::Io(err, string!(path.as_ref().to_string_lossy())))
        .map(std::convert::Into::into)
}

fn read_dir(path: &str) -> InternalResult<fs::ReadDir> {
    fs::read_dir(path).map_err(|err| InternalError::Io(err, string!(path)))
}

#[cfg(test)]
mod tests {
    #![allow(warnings)]

    use super::*;

    use std::sync::Arc;

    #[test]
    fn test_read_file() {
        let mock_path: &str = mock_path!("KERNEL_OSRELEASE/input");
        let expected: &str = mockfs!("KERNEL_OSRELEASE/expected");

        let result: InternalResult<Arc<str>> = read_file(mock_path);

        assert!(result.is_ok());

        assert_eq!(result.unwrap(), expected.into());
    }

    #[test]
    fn test_read_file_error() {
        let mock_path: &str = mock_path!("nonexistant/input");
        let expected: InternalError = InternalError::Io(
            io::Error::new(io::ErrorKind::NotFound, "No such file or directory"),
            string!(mock_path),
        );

        let result: InternalResult<Arc<str>> = read_file(mock_path);

        assert!(result.is_err());

        let err = result.unwrap_err();

        assert!(matches!(err, expected));
    }
}
