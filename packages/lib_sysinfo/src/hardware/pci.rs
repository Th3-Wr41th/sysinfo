// SPDX-License-Identifier: MIT

#![allow(clippy::expect_used)]

use derive_builder::Builder;
use pciid_parser::Database;
use serde::{Deserialize, Serialize};
use std::{fs, io};

use crate::{InternalError, InternalResult};

const PCI_DEVICES: &str = "/sys/bus/pci/devices";

#[derive(Debug, Default, Serialize, Deserialize, Clone, Eq, PartialEq, Hash, Builder)]
#[builder(private, derive(Debug))]
pub struct PCIDevice {
    pub path: String,
    pub class: Class,
    pub id: Id,
    pub subsystem_id: SubId,
    #[builder(default)]
    pub driver: Option<String>,
}

#[derive(Debug, Default, Serialize, Deserialize, Clone, Eq, PartialEq, Hash, Builder)]
#[builder(private, derive(Debug))]
pub struct Class {
    pub raw: String,
    #[builder(default)]
    pub class: Option<String>,
    #[builder(default)]
    pub subclass: Option<String>,
    #[builder(default)]
    pub prog_if: Option<String>,
}

#[derive(Debug, Default, Serialize, Deserialize, Clone, Eq, PartialEq, Hash, Builder)]
#[builder(private, derive(Debug))]
pub struct Id {
    pub raw: String,
    pub vendor: String,
    pub device: String,
}

#[derive(Debug, Default, Serialize, Deserialize, Clone, Eq, PartialEq, Hash, Builder)]
#[builder(private, derive(Debug))]
pub struct SubId {
    pub raw: String,
    pub vendor: String,
    pub device: String,
}

fn parse_data<'a>(
    db: &'a Database,
    builder: &'a mut PCIDeviceBuilder,
    data: &'a [(&'a str, &'a str)],
) -> Option<&'a mut PCIDeviceBuilder> {
    let mut builder = builder;

    for (key, value) in data {
        match *key {
            "PCI_CLASS" => {
                let pci_class = format!("{value:0>6}");

                let chars: Vec<_> = pci_class.chars().collect();

                let data: Vec<String> = chars
                    .chunks(2)
                    .map(|chunk: &[char]| chunk.iter().collect::<String>())
                    .collect();

                let mut class_builder: &mut ClassBuilder = &mut ClassBuilder::default();

                class_builder = class_builder.raw(pci_class);

                if let Some(class) = db.classes.get(&data[0].to_ascii_lowercase()) {
                    class_builder = class_builder.class(Some(class.name.clone()));

                    if let Some(subclass) = class.subclasses.get(&data[1].to_ascii_lowercase()) {
                        class_builder = class_builder.subclass(Some(subclass.name.clone()));

                        if let Some(prog_if) = subclass.prog_ifs.get(&data[2].to_ascii_lowercase())
                        {
                            class_builder = class_builder.prog_if(Some(prog_if.clone()));
                        }
                    }
                };

                let class = class_builder.build().expect("should be valid");

                builder = builder.class(class);
            }
            "PCI_ID" => {
                let mut id_builder: &mut IdBuilder = &mut IdBuilder::default();

                id_builder = id_builder.raw((*value).to_string());

                let Some((vendor_id, device_id)) = value.split_once(':') else {
                    return None;
                };

                if let Some(vendor) = db.vendors.get(&vendor_id.to_ascii_lowercase()) {
                    id_builder = id_builder.vendor(vendor.name.clone());

                    if let Some(device) = vendor.devices.get(&device_id.to_ascii_lowercase()) {
                        id_builder = id_builder.device(device.name.clone());
                    }
                };

                let id = id_builder.build().expect("should be valid");

                builder = builder.id(id);
            }
            "PCI_SUBSYS_ID" => {
                let mut subid_builder: &mut SubIdBuilder = &mut SubIdBuilder::default();

                subid_builder = subid_builder.raw((*value).to_string());

                let Some((vendor_id, device_id)) = value.split_once(':') else {
                    return None;
                };

                if let Some(vendor) = db.vendors.get(&vendor_id.to_ascii_lowercase()) {
                    subid_builder = subid_builder.vendor(vendor.name.clone());

                    if let Some(device) = vendor.devices.get(&device_id.to_ascii_lowercase()) {
                        subid_builder = subid_builder.device(device.name.clone());
                    } else {
                        subid_builder = subid_builder.device(format!("Device {device_id}"));
                    }
                };

                let subid = subid_builder.build().expect("should be valid");

                builder = builder.subsystem_id(subid);
            }
            "PCI_SLOT_NAME" => {
                let path = format!("{PCI_DEVICES}/{value}");

                builder = builder.path(path);
            }
            "DRIVER" => {
                builder = builder.driver(Some((*value).to_string()));
            }
            _ => (),
        }
    }

    Some(builder)
}

pub fn get_devices() -> InternalResult<Vec<PCIDevice>> {
    let dir_iter = read_dir!(PCI_DEVICES)?;
    let db = {
        #[cfg(feature = "pciid-online")]
        {
            Database::get_online().map_err(|err| {
                InternalError::Io(
                    io::Error::new(io::ErrorKind::NotFound, err.to_string()),
                    string!("pci.ids"),
                )
            })
        }

        #[cfg(not(feature = "pciid-online"))]
        {
            Database::read().map_err(|err| {
                InternalError::Io(
                    io::Error::new(io::ErrorKind::NotFound, err.to_string()),
                    string!("pci.ids"),
                )
            })
        }
    }?;

    let devices: Vec<_> = dir_iter
        .filter_map(|dir| {
            if dir.is_err() {
                return None;
            }

            let dir = dir.unwrap();

            let mut path = dir.path();
            path.push("uevent");

            let Ok(content) = fs::read_to_string(path) else {
                return None;
            };

            let mut device_builder: &mut PCIDeviceBuilder = &mut PCIDeviceBuilder::default();

            let data: Vec<(&str, &str)> = content
                .lines()
                .filter_map(|line| line.split_once('='))
                .collect();

            device_builder = parse_data(&db, device_builder, &data)?;

            let device = device_builder.build().expect("should be valid");

            Some(device)
        })
        .collect();

    Ok(devices)
}

#[cfg(test)]
mod tests {
    use super::*;

    use std::collections::HashSet;

    #[test]
    fn test_get_devices() {
        let expected: Vec<PCIDevice> =
            ron!("PCI_DEVICES/expected").expect("failed to parse expected data");

        let result = get_devices();

        assert!(result.is_ok());

        let result = result.unwrap();

        assert_eq!(
            result.iter().collect::<HashSet<_>>(),
            expected.iter().collect::<HashSet<_>>()
        );
    }
}
