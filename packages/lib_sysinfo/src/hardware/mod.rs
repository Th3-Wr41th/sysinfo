// SPDX-License-Identifier: MIT

//! Get information regarding the hardware in the system
//!
//! # Example
//!
//! ```
//! # use lib_sysinfo::hardware::*;
//! let bios = bios_info().unwrap();
//! let cpus: Vec<_> = cpu_info().unwrap();
//!
//! let cpus_formatted: Vec<_> = cpus.iter().map(|cpu| cpu.model.as_ref()).collect();
//!
//! println!("Bios: {0}-{1}", bios.vendor, bios.version,);
//!
//! println!("CPUs: \n{0}{1}", " ".repeat(2), cpus_formatted.join("\n  "));
//! ```

mod pci;
pub mod types;

use pci::PCIDevice;

#[allow(clippy::wildcard_imports)]
use types::*;

use const_format::concatcp;
use std::collections::{HashMap, HashSet};
use std::env::{consts::ARCH, VarError};
use std::sync::Arc;

use crate::{InternalError, InternalResult};

const ROOT: &str = "/sys/class/dmi/id/";

const PRODUCT_FAMILY: &str = concatcp!(ROOT, "product_family");
const SYS_VENDOR: &str = concatcp!(ROOT, "sys_vendor");

const BIOS_DATE: &str = concatcp!(ROOT, "bios_date");
const BIOS_RELEASE: &str = concatcp!(ROOT, "bios_release");
const BIOS_VENDOR: &str = concatcp!(ROOT, "bios_vendor");
const BIOS_VERSION: &str = concatcp!(ROOT, "bios_version");

const CPUINFO: &str = "/proc/cpuinfo";

/// Reads the vendor and product family of the system from `/sys/class/dmi/id`
///
/// # Errors
///
/// Will return an error if it cannot read `/sys/class/dmi/id/sys_vendor` or `/sys/class/dmi/id/product_family`
pub fn host_info() -> InternalResult<HostInfo> {
    let vendor = read!("HOST_INFO/sys_vendor", SYS_VENDOR)?;
    let family = read!("HOST_INFO/product_family", PRODUCT_FAMILY)?;

    Ok(HostInfo { vendor, family })
}

/// Reads bios information from `/sys/class/dmi/id`
///
/// # Errors
///
/// Will return an error if it cannot read `bios_version`, `bios_vendor`, `bios_date` or `bios_release` from `/sys/class/dmi/id`
pub fn bios_info() -> InternalResult<BiosInfo> {
    let vendor = read!("BIOS_INFO/vendor", BIOS_VENDOR)?;
    let version = read!("BIOS_INFO/version", BIOS_VERSION)?;
    let date = read!("BIOS_INFO/date", BIOS_DATE)?;
    let release = read!("BIOS_INFO/release", BIOS_RELEASE)?;

    Ok(BiosInfo {
        vendor,
        version,
        date,
        release,
    })
}

/// Reads CPU information from `/proc/cpuinfo`
///
/// # Errors
///
/// Will return an error if it cannot read `/proc/cpuinfo`
pub fn cpu_info() -> InternalResult<Vec<CpuInfo>> {
    let content: Arc<str> = read!("CPUINFO/input", CPUINFO)?;

    let data: HashSet<CpuInfo> = content
        .split("\n\n")
        .filter_map(|block| {
            let values: HashMap<&str, &str> = block
                .lines()
                .filter_map(|line| {
                    let Some((key, value)) = line.split_once(':') else {
                        return None;
                    };

                    let (key, value) = (key.trim(), value.trim());

                    match key {
                        "model name" => Some(("model", value)),
                        "siblings" => Some(("threads", value)),
                        "cpu cores" => Some(("cores", value)),
                        _ => None,
                    }
                })
                .collect();

            let Some(model) = values.get("model") else {
                return None;
            };

            let Some(cores) = values
                .get("cores")
                .and_then(|cores| cores.parse::<u32>().ok())
            else {
                return None;
            };

            let Some(threads) = values
                .get("threads")
                .and_then(|threads| threads.parse::<u32>().ok())
            else {
                return None;
            };

            Some(CpuInfo {
                model: (*model).into(),
                cores,
                threads,
            })
        })
        .collect();

    Ok(data.iter().map(CpuInfo::clone).collect())
}

/// Get the cpu architecture of the system
///
/// # Errors
///
/// Will return an error if [`ARCH`](`std::env::consts::ARCH`) is not set
pub fn cpu_arch<'a>() -> InternalResult<&'a str> {
    if ARCH.is_empty() {
        Err(InternalError::Env(
            VarError::NotPresent,
            string!("ARCH not set"),
        ))
    } else {
        Ok(ARCH)
    }
}

/// Returns GPUs from `/sys/bus/pci/devices`
///
/// # Errors
///
/// Will return an error if it cannot read the directory `/sys/bus/pci/devices`
pub fn gpu_info() -> InternalResult<Vec<GpuInfo>> {
    let pci_devices: Vec<PCIDevice> = pci::get_devices()?;

    let gpus: Vec<GpuInfo> = pci_devices
        .iter()
        .filter(|device| device.class.raw.starts_with("03"))
        .filter_map(|gpu| {
            // If class is none discard device as it likly malformed
            let _ = gpu.class.class.as_ref()?;

            let mut description: &str = "Unknown";

            if let Some(class) = &gpu.class.class {
                description = class;
            }

            if let Some(subclass) = &gpu.class.subclass {
                description = subclass;
            }

            Some(GpuInfo {
                description: description.into(),
                product: gpu.id.device.clone().into(),
                vendor: gpu.id.vendor.clone().into(),
            })
        })
        .collect();

    Ok(gpus)
}

/// Reads audio devices from `/sys/bus/pci/devices`
///
/// # Errors
///
/// Will return an error if it cannot read the directory `/sys/bus/pci/devices`
pub fn audio_info() -> InternalResult<Vec<AudioInfo>> {
    let pci_devices: Vec<PCIDevice> = pci::get_devices()?;

    let audio_devices: Vec<AudioInfo> = pci_devices
        .iter()
        .filter(|device| device.class.raw.starts_with("0403"))
        .map(|device| AudioInfo {
            product: device.id.device.clone().into(),
            vendor: device.id.vendor.clone().into(),
        })
        .collect();

    Ok(audio_devices)
}

/// Returns networks devices from `/sys/bus/pci/devices`
///
/// # Errors
///
/// Will return an error if it cannot read the directory `/sys/bus/pci/devices`
pub fn network_info() -> InternalResult<Vec<NetworkInfo>> {
    let pci_devices: Vec<PCIDevice> = pci::get_devices()?;

    let devices: Vec<NetworkInfo> = pci_devices
        .iter()
        .filter(|device| device.class.raw.starts_with("02"))
        .map(|device| NetworkInfo {
            product: device.id.device.clone().into(),
            vendor: device.id.vendor.clone().into(),
        })
        .collect();

    Ok(devices)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_host_info() {
        let expected: HostInfo = ron!("HOST_INFO/expected").expect("failed to parse expected data");

        let result = host_info();

        assert!(result.is_ok());

        let result = result.unwrap();

        assert_eq!(result, expected);
    }

    #[test]
    fn test_bios_info() {
        let expected: BiosInfo = ron!("BIOS_INFO/expected").expect("failed to parse expected data");

        let result = bios_info();

        assert!(result.is_ok());

        let result = result.unwrap();

        assert_eq!(result, expected);
    }

    #[test]
    fn test_cpu_info() {
        let expected: Vec<CpuInfo> =
            ron!("CPUINFO/expected").expect("failed to parse expected data");

        let result = cpu_info();

        assert!(result.is_ok());

        let result = result.unwrap();

        assert_eq!(
            result.iter().collect::<HashSet<_>>(),
            expected.iter().collect::<HashSet<_>>()
        );
    }

    #[test]
    fn test_cpu_arch() {
        let result = cpu_arch();

        assert!(result.is_ok());
    }

    #[test]
    fn test_gpu_info() {
        let expected: Vec<GpuInfo> =
            ron!("GPU_INFO/expected").expect("failed to parse expected data");

        let result = gpu_info();

        assert!(result.is_ok());

        let result = result.unwrap();

        assert_eq!(
            result.iter().collect::<HashSet<_>>(),
            expected.iter().collect::<HashSet<_>>()
        );
    }

    #[test]
    fn test_audio_info() {
        let expected: Vec<AudioInfo> =
            ron!("AUDIO_INFO/expected").expect("failed to parse expected data");

        let result = audio_info();

        assert!(result.is_ok());

        let result = result.unwrap();

        assert_eq!(
            result.iter().collect::<HashSet<_>>(),
            expected.iter().collect::<HashSet<_>>()
        );
    }

    #[test]
    fn test_network_info() {
        let expected: Vec<NetworkInfo> =
            ron!("NET_INFO/expected").expect("failed to parse expected data");

        let result = network_info();

        assert!(result.is_ok());

        let result = result.unwrap();

        assert_eq!(
            result.iter().collect::<HashSet<_>>(),
            expected.iter().collect::<HashSet<_>>()
        );
    }
}
