// SPDX-License-Identifier: MIT

//! Various types used to structure data returned from functions

use serde::{Deserialize, Serialize};
use std::sync::Arc;

#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Hash, Clone)]
pub struct HostInfo {
    pub vendor: Arc<str>,
    pub family: Arc<str>,
}

#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Hash, Clone)]
pub struct BiosInfo {
    pub vendor: Arc<str>,
    pub version: Arc<str>,
    pub date: Arc<str>,
    pub release: Arc<str>,
}

#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Hash, Clone)]
pub struct CpuInfo {
    pub model: Arc<str>,
    pub cores: u32,
    pub threads: u32,
}

#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Hash, Clone)]
pub struct GpuInfo {
    pub description: Arc<str>,
    pub product: Arc<str>,
    pub vendor: Arc<str>,
}

#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Hash, Clone)]
pub struct AudioInfo {
    pub product: Arc<str>,
    pub vendor: Arc<str>,
}

#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Hash, Clone)]
pub struct NetworkInfo {
    pub product: Arc<str>,
    pub vendor: Arc<str>,
}
