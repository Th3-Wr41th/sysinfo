// SPDX-License-Identifier: MIT

use serde::{Deserialize, Serialize};
use std::borrow::BorrowMut;
use std::cell::RefCell;
use std::collections::HashMap;
use std::fmt;
use std::fs;
use std::path::PathBuf;
use std::rc::Rc;
use std::{io, sync::Arc};

use crate::disk::statfs::StatFs;
use crate::units::Byte;
use crate::{InternalError, InternalResult};
use unit_system::UnitSystem;

const MTAB: &str = "/etc/mtab";

mod statfs;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Fs {
    pub device: Arc<str>,
    pub mount_path: Arc<str>,
    pub fs_type: FsType,
    pub size: Size,
    pub inodes: INodes,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Size {
    pub total: Byte,
    pub free: Byte,
    pub available: Byte,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct INodes {
    pub total: u64,
    pub free: u64,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub enum FsType {
    Btrfs,
    Ext,
    Fuse,
    Msdos,
    Proc,
    Selinux,
    Sysfs,
    Tmpfs,
    Xfs,
    Unknown,
}

impl fmt::Display for FsType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Btrfs => write!(f, "btrfs"),
            Self::Ext => write!(f, "ext2/ext3/ext4"),
            Self::Fuse => write!(f, "fuse"),
            Self::Msdos => write!(f, "msdos"),
            Self::Proc => write!(f, "procfs"),
            Self::Selinux => write!(f, "selinux"),
            Self::Sysfs => write!(f, "sysfs"),
            Self::Tmpfs => write!(f, "tmpfs"),
            Self::Xfs => write!(f, "xfs"),
            Self::Unknown => write!(f, "unknown"),
        }
    }
}

impl FsType {
    fn from_ftype(fstype: libc::__fsword_t) -> Self {
        match fstype {
            libc::BTRFS_SUPER_MAGIC => Self::Btrfs,
            // Also matches ext3 and ext4
            libc::EXT2_SUPER_MAGIC => Self::Ext,
            libc::FUSE_SUPER_MAGIC => Self::Fuse,
            libc::MSDOS_SUPER_MAGIC => Self::Msdos,
            libc::PROC_SUPER_MAGIC => Self::Proc,
            libc::SELINUX_MAGIC => Self::Selinux,
            libc::SYSFS_MAGIC => Self::Sysfs,
            libc::TMPFS_MAGIC => Self::Tmpfs,
            libc::XFS_SUPER_MAGIC => Self::Xfs,
            _ => Self::Unknown,
        }
    }
}

#[derive(Debug, Default, Clone)]
pub struct FsCache {
    inner: Rc<RefCell<Option<Arc<str>>>>,
}

impl FsCache {
    fn set(&mut self, value: Arc<str>) {
        let new_val = RefCell::new(Some(value));

        *self.inner.borrow_mut() = Rc::new(new_val);
    }

    fn get<T>(&self) -> Option<T>
    where
        T: From<Arc<str>>,
    {
        let inner_val = self.inner.borrow();

        inner_val.clone().map(Into::into)
    }
}

impl Fs {
    fn parse_mtab(cache: &mut FsCache) -> InternalResult<HashMap<Arc<str>, Arc<str>>> {
        let mtab: Arc<str> = if let Some(val) = cache.get::<Arc<str>>() {
            val
        } else {
            let mtab: Arc<str> = read!("MTAB/input", MTAB)?;

            cache.set(mtab.clone());

            mtab
        };

        Ok(mtab
            .lines()
            .filter_map(|fs| {
                let fields: Vec<_> = fs.split(' ').collect();

                if fields.len() != 6 {
                    return None;
                }

                let device: Arc<str> = fields[0].into();
                let mount_path: Arc<str> = fields[1].into();

                Some((mount_path, device))
            })
            .collect())
    }

    pub fn get(path: &str, cache: Option<&mut FsCache>) -> InternalResult<Self> {
        let mut default_cache = FsCache::default();

        let cache: &mut FsCache = cache.unwrap_or(&mut default_cache);

        let filesystems = Self::parse_mtab(cache)?;

        let device: &Arc<str> = filesystems.get(path).ok_or(InternalError::Io(
            io::Error::new(io::ErrorKind::NotFound, format!("Cannot find path: {path}")),
            string!(path),
        ))?;

        let device: Arc<str> = {
            if device.contains('/') {
                let path: PathBuf = fs::canonicalize(device.as_ref())
                    .map_err(|err: io::Error| InternalError::Io(err, string!(device.as_ref())))?;

                path.to_string_lossy().into()
            } else {
                device.clone()
            }
        };

        let stat: StatFs =
            StatFs::new(path).map_err(|err: io::Error| InternalError::Io(err, string!(path)))?;

        let size: Size = {
            let block_size: f64 = stat.block_size() as f64;
            let blocks: f64 = stat.blocks() as f64;
            let free_blocks: f64 = stat.blocks_free() as f64;
            let avail_blocks: f64 = stat.blocks_avail() as f64;

            let total = Byte::new(blocks * block_size);
            let free = Byte::new(free_blocks * block_size);
            let available = Byte::new(avail_blocks * block_size);

            Size {
                total,
                free,
                available,
            }
        };

        let inodes: INodes = {
            let total = stat.inodes();
            let free = stat.inodes_free();

            INodes { total, free }
        };

        Ok(Self {
            device,
            mount_path: path.into(),
            fs_type: FsType::from_ftype(stat.fs_type()),
            size,
            inodes,
        })
    }

    pub fn get_all() -> InternalResult<Vec<Self>> {
        let mut cache = FsCache::default();

        let filesystems = Self::parse_mtab(&mut cache)?;

        Ok(filesystems
            .iter()
            .filter_map(|(mount_path, _)| Self::get(mount_path, Some(&mut cache)).ok())
            .collect())
    }
}

/// Get usage information for mounted filesystems, similar to `df`
///
/// # Errors
///
/// Will return an error if read `/etc/mtab` or parse its contents, if a device
/// path does not exist as specified in `/etc/mtab`, or if `statfs` fails
pub fn get_disk_usage() -> InternalResult<Vec<Fs>> {
    Fs::get_all()
}
