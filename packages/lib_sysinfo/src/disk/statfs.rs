// SPDX-License-Identifier: MIT

use libc::statfs;
use std::ffi::{CString, NulError};
use std::{io, mem::MaybeUninit};

#[derive(Debug)]
pub struct StatFs {
    inner: statfs,
}

impl StatFs {
    pub fn new(path: &str) -> io::Result<Self> {
        let mut statfs_buf = MaybeUninit::uninit();
        let path = CString::new(path)
            .map_err(|err: NulError| io::Error::new(
                io::ErrorKind::NotFound, err.to_string()
            ))?;

        unsafe {
            if statfs(path.as_ptr(), statfs_buf.as_mut_ptr()) == -1 {
                return Err(io::Error::last_os_error());
            }
            Ok(Self {
                inner: statfs_buf.assume_init(),
            })
        }
    }

    pub fn fs_type(&self) -> libc::__fsword_t {
        self.inner.f_type
    }
    
    pub fn block_size(&self) -> libc::__fsword_t {
        self.inner.f_bsize
    }

    pub fn blocks(&self) -> libc::fsblkcnt_t {
        self.inner.f_blocks
    }

    pub fn blocks_free(&self) -> libc::fsblkcnt_t {
        self.inner.f_bfree
    }

    pub fn blocks_avail(&self) -> libc::fsblkcnt_t{
        self.inner.f_bavail
    }
    
    pub fn inodes(&self) -> libc::fsfilcnt_t {
        self.inner.f_files
    }
    
    pub fn inodes_free(&self) -> libc::fsfilcnt_t {
        self.inner.f_ffree
    }
}
