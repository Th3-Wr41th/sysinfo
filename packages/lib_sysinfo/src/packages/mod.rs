// SPDX-License-Identifier: MIT

//! Get information regarding packages on the system

pub mod managers;

use serde::{Deserialize, Serialize};
use strum::EnumIter;

use crate::InternalResult;

use managers::{Cargo, Dpkg, PackageManager as _, Pacman, Rpm};

/// Abstraction of all supported package managers
///
/// # Example
///
/// ```
/// # use lib_sysinfo::packages::*;
/// // Use the rpm package manager
/// let manager = PackageManager::Rpm;
///
/// let res = manager.count();
///
/// match res {
///     Ok(count) => println!("There are {count} rpm packages installed on this system"),
///     Err(_) => println!("Could not get the number of rpm packages installed on this system"),
/// }
/// ```
#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq, EnumIter, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum PackageManager {
    Rpm,
    Pacman,
    Dpkg,
    Cargo,
}

impl PackageManager {
    pub fn name(&self) -> &str {
        match self {
            Self::Rpm => Rpm::NAME,
            Self::Pacman => Pacman::NAME,
            Self::Dpkg => Dpkg::NAME,
            Self::Cargo => Cargo::NAME,
        }
    }

    pub fn count(&self) -> InternalResult<u64> {
        match self {
            Self::Rpm => Rpm.count(),
            Self::Pacman => Pacman.count(),
            Self::Dpkg => Dpkg.count(),
            Self::Cargo => Cargo.count(),
        }
    }

    pub fn updates(&self) -> InternalResult<u64> {
        match self {
            Self::Rpm => Rpm.updates(),
            Self::Pacman => Pacman.updates(),
            Self::Dpkg => Dpkg.updates(),
            Self::Cargo => Cargo.updates(),
        }
    }
}
