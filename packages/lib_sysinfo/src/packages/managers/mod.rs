// SPDX-License-Identifier: MIT

mod cargo;
mod dpkg;
mod pacman;
mod rpm;

#[doc(inline)]
pub use cargo::Cargo;
#[doc(inline)]
pub use dpkg::Dpkg;
#[doc(inline)]
pub use pacman::Pacman;
#[doc(inline)]
pub use rpm::Rpm;

use std::path::PathBuf;
use std::process::Command;
use std::rc::Rc;

use crate::{InternalError, InternalResult};

/// Abstract representation of a package manager
pub trait PackageManager<'a> {
    /// Name of the package manager
    const NAME: &'a str;

    /// Get the path to where the package manager stores packages
    ///
    /// # Errors
    ///
    /// May return an error is package manager is not found on the system
    fn path(&self) -> InternalResult<PathBuf>;

    /// Get the binary for the package manager
    ///
    /// # Errors
    ///
    /// May return an error is package manager is not found on the system
    fn binary(&self) -> InternalResult<Rc<str>>;

    /// Count the packages installed on the system
    ///
    /// # Errors
    ///
    /// May return an error is package manager is not found on the system
    fn count(&self) -> InternalResult<u64>;

    /// Command line arguments passed to the binary
    fn args(&self) -> &'a [&'a str];

    /// Count the numbers of packages with updates on the system, may not be 100% accurate as it
    /// parses the `stdout` of the binary.
    ///
    /// # Errors
    ///
    /// May return an error if it cannot execute a given binary or the given binary exits with a
    /// non-zero code
    fn updates(&self) -> InternalResult<u64> {
        let binary: &str = &self.binary()?;

        cmd(binary, self.args())
    }
}

/// Execute a given binary with its args and return a count of its lines
///
/// # Errors
///
/// May return an error if it cannot execute a given binary or the given
/// binary exits with a non-zero code
pub(super) fn cmd(binary: &str, args: &[&str]) -> InternalResult<u64> {
    let out = Command::new(binary)
        .args(args)
        .output()
        .map_err(|err| InternalError::Io(err, string!(binary)))?;

    if !out.stderr.is_empty() {
        let err = String::from_utf8_lossy(&out.stderr);

        return Err(InternalError::Command(string!(err), string!(binary)));
    }

    let stdout = String::from_utf8_lossy(&out.stdout);

    let count: u64 = stdout
        .split('\n')
        .filter(|line| !line.is_empty())
        .count()
        .try_into()
        .map_err(InternalError::TryFromIntError)?;

    Ok(count)
}

#[cfg(test)]
mod tests {
    use super::*;

    use rstest::rstest;

    #[rstest]
    #[case(Rpm, 10)]
    #[case(Pacman, 1)]
    #[case(Dpkg, 2)]
    #[case(Cargo, 3)]
    fn test_count<'a>(#[case] manager: impl PackageManager<'a>, #[case] expected_count: u64) {
        let result = manager.count();

        assert!(result.is_ok());

        let count = result.unwrap();

        assert_eq!(count, expected_count);
    }
}
