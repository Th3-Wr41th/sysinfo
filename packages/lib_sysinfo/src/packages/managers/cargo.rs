// SPDX-License-Identifier: MIT

use std::fs;
use std::io;
use std::path::{Path, PathBuf};
use std::rc::Rc;

use super::cmd;
use crate::{InternalError, InternalResult};

/// The package manager used for rust [crates](https://doc.rust-lang.org/cargo/appendix/glossary.html#crate)
/// [(details)](https://doc.rust-lang.org/cargo/)
pub struct Cargo;

impl<'a> super::PackageManager<'a> for Cargo {
    const NAME: &'a str = "cargo";

    fn path(&self) -> InternalResult<PathBuf> {
        #[cfg(test)]
        let path: PathBuf = mock_dir!("CARGO_HOME/bin").into();

        #[cfg(not(test))]
        let path: PathBuf = {
            let path_home = home::cargo_home().map_err(InternalError::Packages)?;

            let path_bin = path_home.join("bin");

            let path: &str = &path_bin.to_string_lossy();
            path.into()
        };

        if Path::new(&path).exists() {
            Ok(path)
        } else {
            Err(InternalError::Packages(io::Error::new(
                io::ErrorKind::NotFound,
                string!("Package manager not found on this system"),
            )))
        }
    }

    fn binary(&self) -> InternalResult<Rc<str>> {
        let path: String = {
            let path: PathBuf = self.path()?;

            let bin_path: PathBuf = path.join("cargo-install-update");

            bin_path.to_string_lossy().to_string()
        };

        if Path::new(&path).exists() {
            Ok(path.into())
        } else {
            Err(InternalError::Packages(io::Error::new(
                io::ErrorKind::NotFound,
                format!("No binary found for package manager: {0}", Self::NAME),
            )))
        }
    }

    // May return [Error](`InternalError`) if it cannot read `CARGO_HOME/bin`
    fn count(&self) -> InternalResult<u64> {
        let dir_iter = {
            let path = self.path()?;

            fs::read_dir(path).map_err(|err| InternalError::Io(err, string!("$CARGO_HOME/bin")))?
        };

        let count: u64 = dir_iter
            .count()
            .try_into()
            .map_err(InternalError::TryFromIntError)?;

        Ok(count)
    }

    fn args(&self) -> &'a [&'a str] {
        &["install-update", "-l"]
    }

    fn updates(&self) -> InternalResult<u64> {
        let binary: &str = &self.binary()?;

        let count = cmd(binary, self.args())?;

        // First Line is `Polling ...`
        // Second Line is a header row; not a package
        Ok(count - 2)
    }
}
