// SPDX-License-Identifier: MIT

use std::ffi::OsStr;
use std::fs;
use std::io;
use std::path::{Path, PathBuf};
use std::rc::Rc;

use crate::{InternalError, InternalResult};

const DPKG_DIR: &str = "/var/lib/dpkg/info/";

/// The package manager used by [Debian](https://www.debian.org/) and its derivatives,
/// often through frontends such as [APT](https://en.wikipedia.org/wiki/APT_%28Package_Manager%29)
/// [(details)](https://en.wikipedia.org/wiki/Dpkg)
pub struct Dpkg;

impl<'a> super::PackageManager<'a> for Dpkg {
    const NAME: &'a str = "dpkg";

    fn path(&self) -> InternalResult<PathBuf> {
        #[cfg(test)]
        let path: PathBuf = mock_dir!("DPKG_DIR").into();

        #[cfg(not(test))]
        let path: PathBuf = DPKG_DIR.into();

        if Path::new(&path).exists() {
            Ok(path)
        } else {
            Err(InternalError::Packages(io::Error::new(
                io::ErrorKind::NotFound,
                string!("Package manager not found on this system"),
            )))
        }
    }

    fn binary(&self) -> InternalResult<Rc<str>> {
        let path: &str = "/usr/bin/apt";

        if Path::new(path).exists() {
            Ok(path.into())
        } else {
            Err(InternalError::Packages(io::Error::new(
                io::ErrorKind::NotFound,
                format!("No binary found for package manager: {0}", Self::NAME),
            )))
        }
    }

    fn count(&self) -> InternalResult<u64> {
        let path = self.path()?;

        let dir_iter = fs::read_dir(&path)
            .map_err(|err| InternalError::Io(err, string!(path.to_string_lossy())))?;

        let count: u64 = dir_iter
            .flatten()
            .filter(|entry| entry.path().extension().and_then(OsStr::to_str) == Some("list"))
            .count()
            .try_into()
            .map_err(InternalError::TryFromIntError)?;

        Ok(count)
    }

    fn args(&self) -> &'a [&'a str] {
        &["list", "--upgradable"]
    }
}
