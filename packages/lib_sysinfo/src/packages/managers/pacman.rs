// SPDX-License-Identifier: MIT

use std::fs;
use std::io;
use std::path::{Path, PathBuf};
use std::rc::Rc;

use crate::{InternalError, InternalResult};

const PACMAN_DIR: &str = "/var/lib/pacman/local/";

/// The package manager used by [Arch Linux](https://archlinux.org)
/// and its derivatives [(details)](https://wiki.archlinux.org/title/pacman)
pub struct Pacman;

impl<'a> super::PackageManager<'a> for Pacman {
    const NAME: &'a str = "pacman";

    fn path(&self) -> InternalResult<PathBuf> {
        #[cfg(test)]
        let path: PathBuf = mock_dir!("PACMAN_DIR").into();

        #[cfg(not(test))]
        let path: PathBuf = PACMAN_DIR.into();

        Ok(path)
    }

    fn binary(&self) -> InternalResult<Rc<str>> {
        let path: &str = "/usr/bin/pacman";

        if Path::new(path).exists() {
            Ok(path.into())
        } else {
            Err(InternalError::Packages(io::Error::new(
                io::ErrorKind::NotFound,
                format!("No binary found for package manager: {0}", Self::NAME),
            )))
        }
    }

    fn count(&self) -> InternalResult<u64> {
        let path = self.path()?;


        let dir_iter = fs::read_dir(&path)
            .map_err(|err| InternalError::Io(err, string!(path.to_string_lossy())))?;

        let count: u64 = dir_iter
            .count()
            .try_into()
            .map_err(InternalError::TryFromIntError)?;

        if count > 0 {
            // Ignore ALPM_DB_VERSION
            Ok(count - 1)
        } else {
            Ok(count)
        }
    }

    fn args(&self) -> &'a [&'a str] {
        &["-Sup"]
    }
}
