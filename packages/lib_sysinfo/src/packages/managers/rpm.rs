// SPDX-License-Identifier: MIT

use std::io;
use std::path::{Path, PathBuf};
use std::rc::Rc;

use crate::{InternalError, InternalResult};

const RPM_DB: &str = "/var/lib/rpm/rpmdb.sqlite";

const FRONTENDS: [&str; 2] = ["/usr/bin/dnf", "/usr/bin/yum"];

/// The package manager used by [Red Hat Linux](https://www.redhat.com/en) and its derivatives
/// such as [Fedora](https://fedoraproject.org/) [(details)](https://en.wikipedia.org/wiki/RPM_Package_Manager)
pub struct Rpm;

impl<'a> super::PackageManager<'a> for Rpm {
    const NAME: &'a str = "rpm";

    fn path(&self) -> InternalResult<PathBuf> {
        #[cfg(test)]
        let path: PathBuf = mock_path!("RPM_DB/input").into();

        #[cfg(not(test))]
        let path: PathBuf = RPM_DB.into();

        Ok(path)
    }

    fn binary(&self) -> InternalResult<Rc<str>> {
        let err = Err(InternalError::Packages(io::Error::new(
            io::ErrorKind::NotFound,
            format!("No binary found for package manager: {0}", Self::NAME),
        )));

        let path: &str = {
            let possible_frontends: Vec<&str> = FRONTENDS
                .iter()
                .filter(|frontend| Path::new(frontend).exists())
                .copied()
                .collect();

            if possible_frontends.is_empty() {
                return err;
            }

            possible_frontends[0]
        };

        if Path::new(path).exists() {
            Ok(path.into())
        } else {
            err
        }
    }

    fn count(&self) -> InternalResult<u64> {
        let path = self.path()?;

        let conn: sqlite::Connection = sqlite::open(path).map_err(|err| {
            InternalError::PackageCount(err.message.unwrap_or_else(|| string!("sqlite error")))
        })?;

        let mut statement = conn
            .prepare("SELECT COUNT(*) FROM Installtid")
            .map_err(|err| {
                InternalError::PackageCount(err.message.unwrap_or_else(|| string!("sqlite error")))
            })?;

        if statement.next().is_ok() {
            let count: i64 = statement.read(0).map_err(|err| {
                InternalError::PackageCount(err.message.unwrap_or_else(|| string!("sqlite error")))
            })?;

            #[allow(clippy::cast_sign_loss)]
            Ok(count as u64)
        } else {
            Err(InternalError::PackageCount(string!(
                "Failed to get count from database"
            )))
        }
    }

    fn args(&self) -> &'a [&'a str] {
        &["-q", "check-update"]
    }
}
