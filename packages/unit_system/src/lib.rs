// SPDX-License-Identifier: MIT

use std::fmt;
use std::ops::Deref;

pub use unit_system_macros::{Unit, UnitSystem};

/// Wrapper around a unit to safely move it around.
/// Derefs into the inner value, allowing transparent use of unit methods.
#[derive(Debug, PartialEq, Eq)]
pub struct Units<U: Unit>(pub U);

impl<U: Unit> From<U> for Units<U> {
    fn from(value: U) -> Self {
        Self(value)
    }
}

impl<U: Unit + fmt::Display> fmt::Display for Units<U> {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "{0}", self.0)
    }
}

impl<U: Unit> Deref for Units<U> {
    type Target = U;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

/// Abstraction of a unit of measurement
pub trait Unit: Sized + Copy {
    /// The value used to scale unit prefixes
    const BASE: f64;

    /// The value of a given unit.
    /// For example if it was the second prefix, the value would be `1000` `(BASE ^ 1)`
    fn value(&self) -> f64;

    /// All units within this unit of measurement.
    fn units<'a>() -> &'a [Self];

    /// String representation of a given unit.
    /// For example, given `Iec::KiB`, the string representation would be `"KiB"`
    fn unit<'a>(&self) -> &'a str;
}

/// Abstraction of a unit system
pub trait UnitSystem {
    fn new<V: Into<f64>>(val: V) -> Self;

    /// Construct a new instance from a value with given unit
    fn from_unit<V, U>(value: V, unit: U) -> Self
    where
        V: Into<f64>,
        U: Unit;

    /// Return the inner value
    fn value(&self) -> f64;

    /// Convert to a target unit
    fn as_unit<T: Unit>(&self, target: T) -> (f64, Units<T>) {
        let val = self.value() / target.value();

        (val, Units(target))
    }

    /// Convert to a human readable value
    fn human<U: Unit + Copy>(&self) -> (f64, Units<U>) {
        let units: &[U] = U::units();
        let divisor: f64 = U::BASE;

        let mut value: f64 = self.value();

        let mut i: usize = 0;

        while (value >= divisor) & (i < (units.len() - 1)) {
            value /= divisor;

            i += 1;
        }

        (value, Units(units[i]))
    }
}
