// SPDX-License-Identifier: MIT

#[macro_use]
extern crate unit_system_macros;

use unit_system::{Unit, UnitSystem};

#[derive(Debug, UnitSystem)]
#[unit_system(add, sub)]
struct Byte(f64);

impl std::fmt::Display for Byte {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let (val, unit) = self.human::<IecUnits>();

        write!(f, "{val} {unit}")
    }
}

#[derive(Unit, Copy, Clone)]
#[unit(base = 1024)]
enum IecUnits {
    B,
    KiB,
    MiB,
}

impl std::fmt::Display for IecUnits {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{0}", self.unit())
    }
}

fn main() {
    let val: Byte = Byte::from_unit(0.9, IecUnits::MiB);

    println!("Value: {val:?}"); // => Byte(943718.4)

    let human = val.human::<IecUnits>();

    println!("As Human: {0} {1}", human.0, human.1); // => 921.6 MiB

    let as_mib = val.as_unit(IecUnits::MiB);

    println!("As MiB: {0} {1}", as_mib.0, as_mib.1.unit()); // => 0.9 MiB
}
