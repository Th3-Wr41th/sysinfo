# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v0.3.0] - 2024-01-10

### Added

- `lib_sysinfo`: Make `general::distribution` try and fall back to `env::consts::OS` if it cannot read or parse `/etc/os-release`
- `lib_sysinfo`: Support for getting CPU architecture via `general::cpu_arch`
- `lib_sysinfo`: Support for formatting `general::btime` to the format `hh:mm:ss` via `general::uptime`, to reduce the need for library consumers to implement formatters for this common format
- `lib_sysinfo`: Support for getting load averages via `general::loadavg` and added a display implementation for `LoadAvg` struct to easily format into the common format `1min 5min 15min`
- `lib_sysinfo`: Support for getting the number of running processes via `general::processes`
- `lib_sysinfo`: Support for getting interfaces without IP addresses and interface state to provide better insight to the networking on the system. Added support for sorting interfaces by `ifindex` to produce a more consistent output.
- `lib_sysinfo`: Unit system to allow easy, on the fly conversion of values to specific unit systems. Added implementation of a unit system for converting bytes using IEC units, the units commonly used by the Linux kernel
- `lib_sysinfo`: Support for getting memory and swap usage
- `lib_sysinfo`: Support for getting filesystem usage
- `sysinfo`: Show IP address type before the IP address in display based formatters
- `sysinfo`: Support for displaying load averages
- `sysinfo`: Support for displaying the number of running processes
- `sysinfo`: Support for displaying interface state and interfaces without IP addresses
- `sysinfo`: Support for displaying memory and swap usage information
- `sysinfo`: Support for displaying filesystem usage of common filesystem types (see `lib_sysinfo::disk::FsType`)

### Changed

- `lib_sysinfo`: Specified `Into` type for the `read` macro as `Arc<str>`, as well as making it return an error when it cannot read a file instead of panicking
- `lib_sysinfo`: Refactored to use `Arc<str>` in place of `String`
- `lib_sysinfo`: Changed `general::btime` function return value from `Arc<str>` to `u64`
- `lib_sysinfo`: Visibility of `packages::managers::cmd` to `pub(super)` to prevent leaking internal function
- `lib_sysinfo`: Moved rpm frontends to a const to make it easier to add or remove frontends in the future
- `lib_sysinfo`: Gate modules behind features to allow library consumers to conditionally enable needed modules and with their respective dependencies
- `lib_sysinfo`: Move `Database::read` from `parse_data` to `get_devices` to remove unnecessary reads to `/etc/hwdata/pci.ids`, resulting in significant performance improvements

### Fixed

- `lib_sysinfo`: Replaced pacman args from `-Sy` to `-Sup` as the former requires root privileges and does not actually get a list of packages that require updates

## [v0.2.0] - 2023-12-28

### Added

- `sysinfo`: Support for man page generation
- `lib_sysinfo`: Support for Ipv6 prefixes and support for multiple ips per interface
- `lib_sysinfo`: Serde attributes to `IP` to tag variants with `type`

### Changed

- `lib_sysinfo`: Renamed `IP` in `Interface` to `ip_addresses` to reflect support for multiple IPs per interface
- `lib_sysinfo`: Renamed `netmask` in `Ip::V4` to `cidr` and `netmask` in `Ip::V6` to `prefix`
- `sysinfo`: Refactored data display implementations to use `fmt::Formatter`
- `sysinfo`: Refactored the raw formatter to use `writeln` macros, to make it more extensible and easier to parse
- Updated dependencies for both `sysinfo` and `lib_sysinfo`

### Fixed

- `sysinfo`: Added attributes for serde to fix conditional compilation

### Removed

- `lib_sysinfo`: `if-addrs` crate, replacing it with a custom implementation based on it

## [v0.1.0] - 2023-12-13

Initial Commit

### Added

TODO: Fill in details

[Unreleased]: https://gitlab.com/Th3-Wr41th/sysinfo/compare/v0.3.0...HEAD
[v0.3.0]: https://gitlab.com/Th3-Wr41th/sysinfo/compare/v0.2.0...v0.3.0
[v0.2.0]: https://gitlab.com/Th3-Wr41th/sysinfo/-/compare/v0.1.0...v0.2.0
[v0.1.0]: https://gitlab.com/Th3-Wr41th/sysinfo/-/commit/62983c1237909ba166e9f28fe3438c398c1b9fd6
