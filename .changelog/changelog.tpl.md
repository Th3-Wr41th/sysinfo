# Changelog

All changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

{% for tag in tags -%}
## [{{ tag.name }}] {%- if tag.date != "" %} - {{ tag.date }} {% endif %}

{% for group in tag.commit_groups -%}
### {{ group.scope }}

{% for commit in group.commits -%}
- **{{ commit.id }}** - {{ commit.summary }}
{% endfor %}
{% endfor -%}

{% endfor -%}

{% if remote -%}
{% for tag in tags -%}
{%- if tag.prev_tag -%} 
{%- if tag.name == "Unreleased" -%}
[{{ tag.name }}]: {{ remote }}/compare/{{ tag.prev_tag }}...{{ tag.target_id }}
{% else -%}
[{{ tag.name }}]: {{ remote }}/compare/{{ tag.prev_tag }}...{{ tag.name }}
{% endif -%}
{% else -%}
[{{ tag.name }}]: {{ remote }}/commits/{{ tag.name }}
{% endif -%}
{% endfor -%}
{% endif -%}
